package com.wluotx.hkwxy.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruijie.spl.api.AccountOperationResult;
import com.ruijie.spl.api.FindAllUserTemplatePackagesResult;
import com.ruijie.spl.api.OnlineUserInfo;
import com.ruijie.spl.api.QueryOnlineUserParam;
import com.ruijie.spl.api.QueryOnlineUserResult;
import com.ruijie.spl.api.SamApiBaseResult;
import com.ruijie.spl.api.UserInfo;
import com.ruijie.spl.api.UserTemplatePackageInfo;
import com.wluotx.common.service.RemindService;
import com.wluotx.common.service.UserService;
import com.wluotx.common.service.WebServiceUtils;
import com.wluotx.common.utils.CacheUtils;
import com.wluotx.common.utils.DateUtils;
import com.wluotx.common.utils.NumberUtils;

/**
 * 移动端Controller
 * @author chenJun
 * @version 2017-05-26
 */
@Controller
@RequestMapping(value = "mobile")
public class HkMobileController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private RemindService remindService;
	
	public static final String USER_CACHE = "userCache";
	public static final String USER_CACHE_ID = "userCacheid";
	
	//跳转到首页页面
	@RequestMapping(value = "index")
	public String add(HttpServletRequest request,Model model) {
		return "mobile/index";
	}
	
	//跳转到我的设备页面
	@RequestMapping(value = "device")
	public String device(HttpSession session,Model model) {
		
		UserInfo userInfo = userService.getUserInfo(session);
		
		QueryOnlineUserParam onlineUserParam = new QueryOnlineUserParam();
    	onlineUserParam.setUserId(userInfo.getAccountId());
    	
    	QueryOnlineUserResult onlineUserResult = WebServiceUtils.getSamService().queryOnlineUser(onlineUserParam);
    	
    	OnlineUserInfo[] onlineUserList = onlineUserResult.getOnlineUserInfos();
    	
    	List<Map<String, String>> userList = new ArrayList<Map<String,String>>();
    	
    	if(null != onlineUserList){
    		for(OnlineUserInfo onlineUser:onlineUserList){
    			Map<String, String> map = new HashMap<String, String>();
    			map.put("userId", onlineUser.getAccountId());
    			map.put("onlineTime", DateUtils.formatDateTime(onlineUser.getOnlineTime()));
    			map.put("userIpv4", onlineUser.getUserIpv4());
    			userList.add(map);
        	}
    	}
    	
    	model.addAttribute("onlineUserList", userList);
		return "mobile/device";
	}
	
	//强制用户下线
	@RequestMapping(value = "killOnlineUser")
	@ResponseBody
	public Map<String,Object> killOnlineUser(String userId) {
		Map<String,Object> map = new HashMap<String, Object>();
		
        SamApiBaseResult result = WebServiceUtils.getSamService().kickOnlineUser(userId, 2, 5, null);
        
        map.put("errorCode", result.getErrorCode());
        map.put("errorMessage", result.getErrorMessage());
		
		return map;
	}
	
	//跳转到密码修改页面
	@RequestMapping(value = "toModifyPassword")
	public String toModifyPassword(HttpSession session,Model model) {
		UserInfo userInfo = userService.getUserInfo(session);
		
		String userId = userInfo.getAccountId();
		String userName = userInfo.getUserName();
		
		model.addAttribute("userId", userId);
		model.addAttribute("userName", userName);
		
		return "mobile/modifyPassword";
	}
	
	//验证原密码
	@RequestMapping(value = "verifyPassword")
	@ResponseBody
	public Map<String,Object> verifyPassword(String userId,String password) {
		Map<String,Object> map = new HashMap<String, Object>();
        SamApiBaseResult result = WebServiceUtils.getSamService().verifyPassword(userId, password);
        
        map.put("errorCode", result.getErrorCode());
        map.put("errorMessage", result.getErrorMessage());
		
		return map;
	}
	
	//修改密码
	@RequestMapping(value = "modifyPassword")
	@ResponseBody
	public Map<String,Object> modifyPassword(String userId,String password) {
		Map<String,Object> map = new HashMap<String, Object>();
        SamApiBaseResult result = WebServiceUtils.getSamService().modifyPassword(userId, password);
        
        map.put("errorCode", result.getErrorCode());
        map.put("errorMessage", result.getErrorMessage());
		
		return map;
	}
	
	//跳转到套餐信息页面
	@RequestMapping(value = "packageInfo")
	public String packageInfo(HttpSession session,Model model) {
		
		Map<String,Object> map = new HashMap<String, Object>();
		UserInfo userInfo = userService.getUserInfo(session);
		
		if(null != userInfo && userInfo.getPeriodStartTime()!=null && userInfo.getPeriodLimitTime()!=null){
			Date startDate = userInfo.getPeriodStartTime();
			Date endDate =  DateUtils.getBeforeDay(userInfo.getPeriodLimitTime());
			
			map.put("packageName", userInfo.getPackageName());
			map.put("startDate", DateUtils.formatDate(startDate,"MM-dd"));
			map.put("endDate", DateUtils.formatDate(endDate,"MM-dd"));
			map.put("surplusDay", DateUtils.getSurplusDays(endDate));
			map.put("ratio", NumberUtils.div(DateUtils.getSurplusDays(endDate), DateUtils.getAllDays(startDate, endDate)));
			map.put("accountFee", userInfo.getAccountFee());
			map.put("expireDate", DateUtils.formatDate(endDate,"yyyy-MM-dd"));
		}
		
		model.addAttribute("userInfo", map);
		
		return "mobile/packageInfo";
	}
	
	//跳转到套餐变更页面
	@RequestMapping(value = "changePackage")
	public String changePackage(HttpSession session,Model model) {
		
		UserInfo userInfo = userService.getUserInfo(session);
		
		model.addAttribute("userInfo", userInfo);
		
		//根据用户模板查找相应的套餐
		FindAllUserTemplatePackagesResult allPackagesResult = WebServiceUtils.getSamService().findAllUserTemplatePackages();
		
		List<UserTemplatePackageInfo> packageList = new ArrayList<UserTemplatePackageInfo>();
		
		if(null != allPackagesResult){
			
			UserTemplatePackageInfo[] allPackageInfo = allPackagesResult.getTemplatePackages();
			String atName = userInfo.getAtName();
			
			for(UserTemplatePackageInfo templatePackageInfo:allPackageInfo){
				String userTemplateId = templatePackageInfo.getUserTemplateId();
				if(atName.equals(userTemplateId)){
					packageList.add(templatePackageInfo);
				}
			}
		}
		
		model.addAttribute("packageList", packageList);
		
		return "mobile/changePackage";
	}
	
	//变更用户套餐
	@RequestMapping(value = "changeUserPackage")
	@ResponseBody
	public Map<String,Object> changeUserPackage(HttpSession session,String userId,String onPackageName,String userTemplateName,String userPackageName,int operationTimeType) {
		Map<String,Object> map = new HashMap<String, Object>();
		UserInfo userInfo = userService.getUserInfo(session);
		SamApiBaseResult result = WebServiceUtils.getSamService().modifyUserPackage(userId,userTemplateName,userPackageName,operationTimeType);
        
        map.put("errorCode", result.getErrorCode());
        map.put("errorMessage", result.getErrorMessage());
        map.put("onPackageName", onPackageName);
        map.put("userPackageName", userPackageName);
        map.put("activeDate", DateUtils.formatDate(userInfo.getPeriodLimitTime(),"yyyy-MM-dd"));
		
		return map;
	}
	
	//跳转到充值卡充值页面
	@RequestMapping(value = "chargeCard")
	public String chargeCard(HttpSession session,Model model) {
		
		UserInfo userInfo = userService.getUserInfo(session);
		
		model.addAttribute("userInfo", userInfo);
		
		return "mobile/chargeCard";
	}
	
	//充值卡充值
	@RequestMapping(value = "chargeUserCard")
	@ResponseBody
	public Map<String,Object> chargeUserCard(String userId,String cardNo,String password) {
		Map<String,Object> map = new HashMap<String, Object>();
		String txid = WebServiceUtils.getSamService().createTxid();
		String chargeCardType = "0";  //默认为0：余额充值         1：待扣款充值
		AccountOperationResult result = WebServiceUtils.getSamService().ChargeCard(userId,chargeCardType,cardNo,password,txid);
		
		//执行成功剔除userInfo
		if(result.getErrorCode()==0){
			CacheUtils.remove(USER_CACHE, USER_CACHE_ID);
		}
        
        map.put("errorCode", result.getErrorCode());
        map.put("errorMessage", result.getErrorMessage());
        map.put("accountInfo", result.getAccountInfo());
		
		return map;
	}
	
	//跳转到提醒页面
	@RequestMapping(value = "xywRemind")
	public String xywRemind(HttpSession session,Model model) {
		UserInfo userInfo = userService.getUserInfo(session);
		Map<String, Object> map=remindService.qryXywRemind(userInfo.getUserId());
		
		model.addAttribute("userInfo", userInfo);
		model.addAttribute("remindInfo", map);
		System.out.println("user:"+userInfo.getUserName());
		return "mobile/xywRemind";
	}
	
	@RequestMapping(value = "updateRemind")
	@ResponseBody
	public Map<String,Object> updateRemind(HttpSession session,String remind,String beforeDays) {
		String userId = (String) session.getAttribute("userId");
		
		Map<String,Object> map = new HashMap<String, Object>();
		
		remindService.addXywRemind(userId, remind, beforeDays);
		
		return map;
	}
	
	
}