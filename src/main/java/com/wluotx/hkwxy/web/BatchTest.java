package com.wluotx.hkwxy.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wluotx.batch.XywBatch;

@Controller
public class BatchTest {
	
	@Autowired
	private XywBatch xywBatch;
	
	
	@RequestMapping(value = "xywBatch")
	@ResponseBody
	public String xywBatch(HttpServletRequest request,String remind,String beforeDays) {

		xywBatch.sendMessage();
		
		return null;
	}
	
	
}