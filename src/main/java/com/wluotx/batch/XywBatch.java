package com.wluotx.batch;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ruijie.spl.api.UserInfo;
import com.wluotx.common.service.Router;
import com.wluotx.common.service.UserService;

@Service("XywBatch")
public class XywBatch {
	protected Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private Router router;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private UserService userService;
	
	protected SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日");
	
	@PostConstruct
	public void init(){
		
	    //每天18点发送微信提醒
		Calendar calendar = Calendar.getInstance();
		if(calendar.get(Calendar.HOUR_OF_DAY)>=20)
			calendar.add(Calendar.DAY_OF_YEAR, 1);
	    calendar.set(Calendar.HOUR_OF_DAY, 20); // 控制时
	    calendar.set(Calendar.MINUTE, 0);    // 控制分
	    calendar.set(Calendar.SECOND, 0);    // 控制秒
	    Date qryTime1 = calendar.getTime();
	 
	    Timer sendTimer = new Timer();
	    sendTimer.scheduleAtFixedRate(new TimerTask() {
	    	public void run() {
	    		sendMessage();
	    	}
	    }, qryTime1, 1000 * 60 * 60 * 24);// 这里设定将延时每天固定执行
	    
	}
	
	public void sendMessage(){
		String sql="select * from xywRemind where remind='on'";
		List<Map<String,Object>> list=jdbcTemplate.queryForList(sql);
		
		long startTime=System.currentTimeMillis();
		for(int i=0;i<list.size();i++){
			//每分钟最多发200条，减轻sam服务器压力
			if(i%200==0){
				long now=System.currentTimeMillis();
				if((now-startTime)<60*1000){//若近200条耗时小于1分钟，则等待至1分钟
					try {
						Thread.sleep(60*1000-(now-startTime));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				startTime=now;
			}
			Map<String,Object> map=list.get(i);
			String userId=(String) map.get("USERID");
			BigDecimal beforeDays=(BigDecimal) map.get("BEFOREDAYS");
			
			UserInfo userInfo = userService.getUserInfo(userId);
			Date limitTime=userInfo.getPeriodLimitTime();
			
			if(limitTime==null)
				continue;
			
			//提醒日期
			Date remindDate=new Date();
			remindDate.setTime(limitTime.getTime()-(long)beforeDays.intValue()*3600*24*1000);
//			remindDate.setTime(limitTime.getTime()-(long)51*3600*24*1000);
			
			//今天
			Date today=new Date();
			try {
				remindDate=sdf.parse(sdf.format(remindDate));
				today=sdf.parse(sdf.format(today));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			//相等发推送
			if(remindDate.getTime()==today.getTime()){
				String desc="亲爱的"+userInfo.getUserName()+"您好,华中大微校园提醒您，您的校园网套餐将于"
						+sdf.format(limitTime)+"到期，请及时续费！";
				router.sendWxMessage(userId, 
					"您的校园网套餐即将到期", desc, 
					"https://m.hust.edu.cn/hkwxy/mobile/packageInfo");
			}

		}
			
		log.error("发送到期提醒："+new Date());
	}
}
