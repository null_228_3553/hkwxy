package com.wluotx.common.utils;

import java.beans.PropertyDescriptor;
import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class Util {
	private final static String iv = "01234567";
	private final static String encoding = "utf-8";
	private static  Log log = LogFactory.getLog(Util.class);
	
	public static String getMD5(String str) {
	    try {
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        md.update(str.getBytes());
	        return new BigInteger(1, md.digest()).toString(16);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return null;
	    }
	}
	
	public static String getUrlParam(Map<String,String> map){
		if(map==null){
			return "";
		}
		StringBuffer ret=new StringBuffer();
		Iterator<String> it=map.keySet().iterator();
		while(it.hasNext()){
			String key=it.next();
			ret.append("&"+key+"="+map.get(key));
		}
		return ret.toString().substring(1);
	}
	
	public static Map<String,Object> bean2Map(Object beanObject){
        BeanWrapperImpl bean = new BeanWrapperImpl(beanObject);
        PropertyDescriptor desc[] = bean.getPropertyDescriptors();
        Map<String,Object> dataMap = new HashMap<String,Object>(desc.length);
        try{
            for(int i = 0; i < desc.length; i++) {
                String name = desc[i].getName();
                if(bean.isWritableProperty(name) && bean.isReadableProperty(name)) {
                    Object object = bean.getPropertyValue(name);
                    if(object != null) {
//                        String convertedName = (new StringBuilder(String.valueOf(Character.toUpperCase(name.charAt(0))))).append(name.substring(1)).toString();
                        dataMap.put(name, object);
                    }
                }
            }
            return dataMap;
        }catch(Exception e1){
        	log.error(e1);
            throw new RuntimeException(e1);
        }
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object map2Bean(Map<String,Object> map, Class clazz) {
		BeanWrapper bw = new BeanWrapperImpl(clazz);
		PropertyDescriptor props[] = bw.getPropertyDescriptors();
		PropertyDescriptor apropertydescriptor[];
		int j = (apropertydescriptor = props).length;
		for(int i = 0; i < j; i++) {
			PropertyDescriptor pd = apropertydescriptor[i];
			String name = pd.getName();
			if(bw.isWritableProperty(name) && bw.isReadableProperty(name)) {
				Class class0 = pd.getPropertyType();
				if(class0.isAssignableFrom(java.lang.Enum.class)) {
					String convertedName = (new StringBuilder(String.valueOf(Character.toUpperCase(name.charAt(0))))).append(name.substring(1)).toString();
					Object value = map.get(convertedName);
					if(value != null)
						if(value.getClass() == class0) {
							bw.setPropertyValue(name, value);
						} else {
							String enumValue = String.valueOf(value);
							if(enumValue.length() > 0)
							{
								Enum v = Enum.valueOf(class0, String.valueOf(value));
								bw.setPropertyValue(name, v);
							}
						}
				} else {
					//key首字母小写
					Object value = map.get(name);
					if(value==null){
						//key首字母大写
						String convertedName = (new StringBuilder(String.valueOf(Character.toUpperCase(name.charAt(0))))).append(name.substring(1)).toString();
						value = map.get(convertedName);
					}
					if(value != null){
						bw.setPropertyValue(name, value);
					}
				}
			}
		}
		return bw.getWrappedInstance();
	}
	public static String desEncode(String plainText,String pk){
		byte[] encryptData=null;
		try {
			DESedeKeySpec spec = new DESedeKeySpec(pk.getBytes());
			SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
			Key deskey = keyfactory.generateSecret(spec);

			Cipher cipher = Cipher.getInstance("desede/CBC/PKCS5Padding");
			IvParameterSpec ips = new IvParameterSpec(iv.getBytes());
			cipher.init(Cipher.ENCRYPT_MODE, deskey, ips);
			encryptData = cipher.doFinal(plainText.getBytes(encoding));

		} catch (Exception e) {
			log.error(e);;
		} 
		return Bytes2HexString(encryptData);
	}
	public static String Bytes2HexString(byte[] b) {
		String ret = "";
		for (int i = 0; i < b.length; i++) {
			String hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			ret += hex.toUpperCase();
		}
		return ret;
	}
	public static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

}
