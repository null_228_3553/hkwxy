package com.wluotx.common.service;


import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.ruijie.spl.api.QueryUserParams;
import com.ruijie.spl.api.QueryUserResult;
import com.ruijie.spl.api.UserInfo;


@Service("userService")
public class UserService{
	
	public static final String USER_CACHE = "userCache";
	public static final String USER_CACHE_ID = "userCacheid";

	public UserInfo getUserInfo(String userId){
		UserInfo userInfo=null;
//		userInfo = (UserInfo)CacheUtils.get(USER_CACHE, USER_CACHE_ID);
//		if(null == userInfo){
			QueryUserParams params = new QueryUserParams();
			params.setUserId(userId);
			
			QueryUserResult userResult = WebServiceUtils.getSamService().queryUser(params);
			
			UserInfo[] userInfoList = userResult.getData();
			
			if(null != userInfoList){
				userInfo = userInfoList[0];
//				CacheUtils.put(USER_CACHE, USER_CACHE_ID, userInfo);
			}
		/*}else if(!userId.equals(userInfo.getAccountId())){
			CacheUtils.remove(USER_CACHE, USER_CACHE_ID);
			
			QueryUserParams params = new QueryUserParams();
			params.setUserId(userId);
			
			QueryUserResult userResult = WebServiceUtils.getSamService().queryUser(params);
			
			UserInfo[] userInfoList = userResult.getData();
			
			if(null != userInfoList){
				userInfo = userInfoList[0];
				CacheUtils.put(USER_CACHE, USER_CACHE_ID, userInfo);
			}
		}*/
		return userInfo;
	}
	public UserInfo getUserInfo(HttpSession session){
		if(session.getAttribute("UserInfo")==null){
			String userId = (String)session.getAttribute("userId");
			UserInfo userInfo=getUserInfo(userId);
			session.setAttribute("UserInfo", userInfo);
		}
		return (UserInfo)session.getAttribute("UserInfo");
	}
}
