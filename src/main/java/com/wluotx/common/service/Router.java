package com.wluotx.common.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.wluotx.common.utils.Util;

@Service("router")
public class Router {
	private Log log = LogFactory.getLog(getClass());
	//接口地址
	private static final String url="http://one.hust.edu.cn/dcp/ifs";
	//接口超时时间30秒
	private static int timeout=30*1000;
	//秘钥
	private static String privateKey="abcdefghijklmnopqrs12345";
	//系统名
	private static String sysId="elecFee";
	
	public void sendWxMessage(String userId,String title,String desc,String remindLink){

		String param=MessageFormat.format(""
				+ "sysid={0}&module=remind&function=sendWxMessage&receiveUser={1}&title={2}"
				+ "&description={3}&remindLink={4}",
				sysId,userId,title,desc,remindLink);
		String encodeParam=Util.desEncode(param,privateKey);
		String allParam="sysid="+sysId+"&param="+encodeParam;
		String res=sendPost(url,allParam);
		log.error(res);
	}
	
	public String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "application/json, text/javascript, */*; q=0.01");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36");
            conn.setReadTimeout(timeout);
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }
	

}
