package com.wluotx.common.service;


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


@Service("remindService")
public class RemindService{
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	
	public Map<String, Object> qryXywRemind(String userId) {
		String sql="select * from xywRemind where userId=? ";
		try{
			Map<String,Object> retMap=jdbcTpltCore.queryForMap(sql, new Object[]{userId});
			return retMap;
		}catch(EmptyResultDataAccessException e){
			return new HashMap<String,Object>();
		}
	}

	public void addXywRemind(String userId, String remind, String beforeDays) {
		String sql="MERGE INTO xywRemind t1 "
		    +"USING(SELECT ? userid,? remind,? beforeDays FROM dual) t2 "
		    +"ON(t1.userid=t2.userid) "
		    +"WHEN MATCHED THEN "
		    +"update set t1.remind=t2.remind,t1.beforeDays=t2.beforeDays where t1.userid=t2.userid "
		    +"WHEN NOT MATCHED THEN "
		    +"insert(userid,remind,beforeDays) VALUES(t2.userid,t2.remind,t2.beforeDays)";
		jdbcTpltCore.update(sql,new Object[]{userId,remind,beforeDays});
		return;
	}
}
