package com.wluotx.common.service;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.bus.CXFBusFactory;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import com.ruijie.spl.api.SamService;
import com.wluotx.common.config.Global;
import com.wluotx.common.utils.CacheUtils;

/**
 * 连接WebService接口类
 * @author chenjun
 * @createDate 2017年6月28日
 */
public class WebServiceUtils {
	
	public static final String SAM_CACHE = "samCache";
	public static final String SAM_CACHE_ID = "samCacheid";
	
	public static SamService getSamService() {
		SamService samService = (SamService)CacheUtils.get(SAM_CACHE, SAM_CACHE_ID);
		
		if(null == samService){
			String username = Global.getConfig("username");
			String password = Global.getConfig("password");
			String address = Global.getConfig("address");
			String wsdllocation = Global.getConfig("wsdllocation");
			
			CXFBusFactory busFactory = new CXFBusFactory();
	        Bus bus = busFactory.createBus();
	        BusFactory.setThreadDefaultBus(bus);
	        ClientProxyFactoryBean factory = new ClientProxyFactoryBean();
	        factory.setBus(bus);
	        factory.setUsername(username);
	        factory.setPassword(password);
	        factory.setServiceClass(SamService.class);
	        factory.setAddress(address);
	        factory.setWsdlLocation(wsdllocation);
	        
			samService = (SamService)factory.create();
			CacheUtils.put(SAM_CACHE, SAM_CACHE_ID, samService);
		}
		
		return samService;
	}
}
