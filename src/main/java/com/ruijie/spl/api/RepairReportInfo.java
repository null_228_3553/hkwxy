package com.ruijie.spl.api;

import java.util.Date;

public class RepairReportInfo {
	// 报修单编号
	private String repairReportNo;

	// 校区
	private String district;

	// 楼号
	private String buildingNo;

	// 房间号
	private String roomNo;

	// 报修人
	private String userName;

	// 联系电话
	private String phone;

	// 预约时间
	private String bookingTime;

	// 登录名
	private String userId;

	// 连接状态
	private Short connectState;

	// 客户端提示信息
	private Short alertId;

	private String userIp;

	private String userMac;

	private String netMask;

	private String gateway;

	// 寝室联网
	private Short roomNet;

	// 曾经联网
	private Short usedNet;

	// 网速情况
	private Short netSpeed;

	// 端口状态
	private Short portState;

	// ping状态
	private Short pingState;

	// 报修时间
	private Date createTime;

	// 问题描述
	private String description;

	// 审核人
	private String checker;

	// 审核原因
	private String checkDesc;

	// 审核时间
	private Date checkTime;

	// 处理人
	private String dealer;

	// 处理原因
	private String dealDesc;

	// 处理时间
	private Date dealTime;
	
	//是否公开
	private boolean isPublic;

	// 当前状态信息
	private Short state;
	
	private String apLocation; //故障发生地点
	
	private boolean isAccessTypeWideless;  //无线接入方式 
	
	private boolean isAccessTypeWired; //有线接入方式
	
	private boolean isAccessTypeRuijieSu; //SU接入方式
	
	private boolean isAccessTypeWeb; //web接入方式
	
	private boolean isAccessTypeVpn; //vpn接入方式
	
	private boolean isAccessTypeOther; //其他接入方式
	
	private String userIpv6;
	
	private Short  userProtocol;
	
	private Short netStatus;
	
	private String gatewayIpv6;
	
	private Short pingStateIpv6;
	
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	public String getRepairReportNo() {
		return repairReportNo;
	}
	public void setRepairReportNo(String repairReportNo) {
		this.repairReportNo = repairReportNo;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBuildingNo() {
		return buildingNo;
	}
	public void setBuildingNo(String buildingNo) {
		this.buildingNo = buildingNo;
	}
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getBookingTime() {
		return bookingTime;
	}
	public void setBookingTime(String bookingTime) {
		this.bookingTime = bookingTime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Short getConnectState() {
		return connectState;
	}
	public void setConnectState(Short connectState) {
		this.connectState = connectState;
	}
	public Short getAlertId() {
		return alertId;
	}
	public void setAlertId(Short alertId) {
		this.alertId = alertId;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
	public String getUserMac() {
		return userMac;
	}
	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}
	public String getNetMask() {
		return netMask;
	}
	public void setNetMask(String netMask) {
		this.netMask = netMask;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public Short getRoomNet() {
		return roomNet;
	}
	public void setRoomNet(Short roomNet) {
		this.roomNet = roomNet;
	}
	public Short getUsedNet() {
		return usedNet;
	}
	public void setUsedNet(Short usedNet) {
		this.usedNet = usedNet;
	}
	public Short getNetSpeed() {
		return netSpeed;
	}
	public void setNetSpeed(Short netSpeed) {
		this.netSpeed = netSpeed;
	}
	public Short getPortState() {
		return portState;
	}
	public void setPortState(Short portState) {
		this.portState = portState;
	}
	public Short getPingState() {
		return pingState;
	}
	public void setPingState(Short pingState) {
		this.pingState = pingState;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getChecker() {
		return checker;
	}
	public void setChecker(String checker) {
		this.checker = checker;
	}
	public String getCheckDesc() {
		return checkDesc;
	}
	public void setCheckDesc(String checkDesc) {
		this.checkDesc = checkDesc;
	}
	public Date getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}
	public String getDealer() {
		return dealer;
	}
	public void setDealer(String dealer) {
		this.dealer = dealer;
	}
	public String getDealDesc() {
		return dealDesc;
	}
	public void setDealDesc(String dealDesc) {
		this.dealDesc = dealDesc;
	}
	public Date getDealTime() {
		return dealTime;
	}
	public void setDealTime(Date dealTime) {
		this.dealTime = dealTime;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	public Short getState() {
		return state;
	}
	public void setState(Short state) {
		this.state = state;
	}
	public String getApLocation() {
		return apLocation;
	}
	public void setApLocation(String apLocation) {
		this.apLocation = apLocation;
	}
	public boolean isAccessTypeWideless() {
		return isAccessTypeWideless;
	}
	public void setAccessTypeWideless(boolean isAccessTypeWideless) {
		this.isAccessTypeWideless = isAccessTypeWideless;
	}
	public boolean isAccessTypeWired() {
		return isAccessTypeWired;
	}
	public void setAccessTypeWired(boolean isAccessTypeWired) {
		this.isAccessTypeWired = isAccessTypeWired;
	}
	public boolean isAccessTypeRuijieSu() {
		return isAccessTypeRuijieSu;
	}
	public void setAccessTypeRuijieSu(boolean isAccessTypeRuijieSu) {
		this.isAccessTypeRuijieSu = isAccessTypeRuijieSu;
	}
	public boolean isAccessTypeWeb() {
		return isAccessTypeWeb;
	}
	public void setAccessTypeWeb(boolean isAccessTypeWeb) {
		this.isAccessTypeWeb = isAccessTypeWeb;
	}
	public boolean isAccessTypeVpn() {
		return isAccessTypeVpn;
	}
	public void setAccessTypeVpn(boolean isAccessTypeVpn) {
		this.isAccessTypeVpn = isAccessTypeVpn;
	}
	public boolean isAccessTypeOther() {
		return isAccessTypeOther;
	}
	public void setAccessTypeOther(boolean isAccessTypeOther) {
		this.isAccessTypeOther = isAccessTypeOther;
	}
	public String getUserIpv6() {
		return userIpv6;
	}
	public void setUserIpv6(String userIpv6) {
		this.userIpv6 = userIpv6;
	}
	public Short getUserProtocol() {
		return userProtocol;
	}
	public void setUserProtocol(Short userProtocol) {
		this.userProtocol = userProtocol;
	}
	public Short getNetStatus() {
		return netStatus;
	}
	public void setNetStatus(Short netStatus) {
		this.netStatus = netStatus;
	}
	public String getGatewayIpv6() {
		return gatewayIpv6;
	}
	public void setGatewayIpv6(String gatewayIpv6) {
		this.gatewayIpv6 = gatewayIpv6;
	}
	public Short getPingStateIpv6() {
		return pingStateIpv6;
	}
	public void setPingStateIpv6(Short pingStateIpv6) {
		this.pingStateIpv6 = pingStateIpv6;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
	public String getField13() {
		return field13;
	}
	public void setField13(String field13) {
		this.field13 = field13;
	}
	public String getField14() {
		return field14;
	}
	public void setField14(String field14) {
		this.field14 = field14;
	}
	public String getField15() {
		return field15;
	}
	public void setField15(String field15) {
		this.field15 = field15;
	}
	public String getField16() {
		return field16;
	}
	public void setField16(String field16) {
		this.field16 = field16;
	}
	public String getField17() {
		return field17;
	}
	public void setField17(String field17) {
		this.field17 = field17;
	}
	public String getField18() {
		return field18;
	}
	public void setField18(String field18) {
		this.field18 = field18;
	}
	public String getField19() {
		return field19;
	}
	public void setField19(String field19) {
		this.field19 = field19;
	}
	public String getField20() {
		return field20;
	}
	public void setField20(String field20) {
		this.field20 = field20;
	}

}
