package com.ruijie.spl.api;

public class FindAllCustomFieldResult extends SamApiBaseResult{
private UserFieldInfo[] userFieldInfos;

public FindAllCustomFieldResult(){
	
}

public FindAllCustomFieldResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setUserFieldInfos(null);
}

public FindAllCustomFieldResult(int errorCode,String errorMessage,UserFieldInfo[] userFieldInfos){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setUserFieldInfos(userFieldInfos);
}

public UserFieldInfo[] getUserFieldInfos() {
	return userFieldInfos;
}

public void setUserFieldInfos(UserFieldInfo[] userFieldInfos) {
	this.userFieldInfos = userFieldInfos;
}

}
