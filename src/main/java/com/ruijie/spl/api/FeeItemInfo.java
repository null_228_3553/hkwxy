package com.ruijie.spl.api;

import java.math.BigDecimal;

public class FeeItemInfo implements java.io.Serializable{

	

	private String name;
	private String value;
	private BigDecimal timeOrFlowValue = new BigDecimal(0);
	
	private Short chargeType;	
	private String ruleInfoUuid;
	private String ruleInfoDesc;
	
	private BigDecimal fee = new BigDecimal(0);
	

	/**
	 * 
	 */
	public FeeItemInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param name
	 * @param value
	 * @param chargeType
	 * @param chargeTypeDesc
	 * @param ruleInfoUuid
	 * @param ruleInfoDesc
	 */
	public FeeItemInfo(String name, String value, Short chargeType,String ruleInfoUuid, String ruleInfoDesc) {
		super();
		this.name = name;
		this.value = value;
		this.chargeType = chargeType;
		this.ruleInfoUuid = ruleInfoUuid;
		this.ruleInfoDesc = ruleInfoDesc;
	}
	
	/**
	 * @param name
	 * @param value
	 * @param chargeType
	 */	
	public FeeItemInfo(String name, String value, Short chargeType) {
		super();
		this.name = name;
		this.value = value;
		this.chargeType = chargeType;
	}
	
	/**
	 * @param name
	 * @param value
	 * @param chargeType
	 * @param ruleInfoUuid
	 * @param ruleInfoDesc
	 * @param fee
	 */
	public FeeItemInfo(String name, String value, Short chargeType,
			String ruleInfoUuid, String ruleInfoDesc, BigDecimal fee) {
		super();
		this.name = name;
		this.value = value;
		this.chargeType = chargeType;
		this.ruleInfoUuid = ruleInfoUuid;
		this.ruleInfoDesc = ruleInfoDesc;
		this.fee = fee;
	}
	
	//修改华北科大反馈问题
	public boolean getIsUsed(){
		if(this.getValue().equals("未使用")){
			return false;
		}
		return true;
	}
	
	public BigDecimal getTimeOrFlowValue() {
		return timeOrFlowValue;
	}
	public void setTimeOrFlowValue(BigDecimal timeOrFlowValue) {
		this.timeOrFlowValue = timeOrFlowValue;
	}
	public BigDecimal getFee() {
		if(fee != null)
			return fee.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		return fee;
	}
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Short getChargeType() {
		return chargeType;
	}
	public void setChargeType(Short chargeType) {
		this.chargeType = chargeType;
	}
	public String getRuleInfoUuid() {
		return ruleInfoUuid;
	}
	public void setRuleInfoUuid(String ruleInfoUuid) {
		this.ruleInfoUuid = ruleInfoUuid;
	}
	public String getRuleInfoDesc() {
/*		if(StringUtil.isNotEmpty(ruleInfoDesc))
			return TermUtil.getTermStr(ruleInfoDesc);*/
		
		return ruleInfoDesc;
	}
	public void setRuleInfoDesc(String ruleInfoDesc) {
		this.ruleInfoDesc = ruleInfoDesc;
	}

}
