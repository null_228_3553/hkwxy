package com.ruijie.spl.api;

public class QueryOnlineUserResultV2 extends SamApiBaseResult{
	private int total;
	private OnlineUserInfoV2[] onlineUserInfos;
	
	public QueryOnlineUserResultV2(){
		
	}
	
	public QueryOnlineUserResultV2(int errorCode, String errorMessage) {
		super(errorCode,errorMessage);
	}
	
	public QueryOnlineUserResultV2(int errorCode, String errorMessage, int total, OnlineUserInfoV2[] onlineUserInfos) {
		super(errorCode,errorMessage);
		this.total = total;
		this.onlineUserInfos = onlineUserInfos;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public OnlineUserInfoV2[] getOnlineUserInfos() {
		return onlineUserInfos;
	}
	public void setOnlineUserInfos(OnlineUserInfoV2[] onlineUserInfos) {
		this.onlineUserInfos = onlineUserInfos;
	}
}
