package com.ruijie.spl.api;

import java.math.BigDecimal;

public class FindChargeCardResult extends SamApiBaseResult{
	private BigDecimal cardValue;
	private int cardState;
	public FindChargeCardResult(){
		
	}
	
	public FindChargeCardResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.cardState=0;
		this.cardValue=new BigDecimal(0);
	}
	
	public FindChargeCardResult(int errorCode,String errorMessage,int cardState,BigDecimal cardValue){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.cardState=cardState;
		this.cardValue=cardValue;
	}
	public int getCardState() {
		return cardState;
	}
	public void setCardState(int cardState) {
		this.cardState = cardState;
	}
	public BigDecimal getCardValue() {
		return cardValue;
	}
	public void setCardValue(BigDecimal cardValue) {
		this.cardValue = cardValue;
	}

}
