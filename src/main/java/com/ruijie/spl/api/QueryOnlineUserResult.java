package com.ruijie.spl.api;

public class QueryOnlineUserResult extends SamApiBaseResult{
	private int total;
	private OnlineUserInfo[] onlineUserInfos;
	
	public QueryOnlineUserResult(){
		
	}
	
	public QueryOnlineUserResult(int errorCode, String errorMessage) {
		super(errorCode,errorMessage);
	}
	
	public QueryOnlineUserResult(int errorCode, String errorMessage, int total, OnlineUserInfo[] onlineUserInfos) {
		super(errorCode,errorMessage);
		this.total = total;
		this.onlineUserInfos = onlineUserInfos;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public OnlineUserInfo[] getOnlineUserInfos() {
		return onlineUserInfos;
	}
	public void setOnlineUserInfos(OnlineUserInfo[] onlineUserInfos) {
		this.onlineUserInfos = onlineUserInfos;
	}
}
