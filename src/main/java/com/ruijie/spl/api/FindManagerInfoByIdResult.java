package com.ruijie.spl.api;

public class FindManagerInfoByIdResult extends SamApiBaseResult{
	private UserInfo userinfo;
	
	public FindManagerInfoByIdResult(){
		
	}
	
	public FindManagerInfoByIdResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.setUserinfo(null);
	}
	
	public FindManagerInfoByIdResult(int errorCode,String errorMessage,UserInfo userinfo){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.setUserinfo(userinfo);
	}

	public UserInfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(UserInfo userinfo) {
		this.userinfo = userinfo;
	}
}
