package com.ruijie.spl.api;

import java.math.BigDecimal;
import java.util.Date;


public interface SamService {
	//ww add
		public SamApiBaseResult addInhibitV2(AddInhibitParamsV2 params);
		
		public SamApiBaseResult addInhibit(AddInhibitParams params);
		/*
		 * 删除黑名单
		 * modified by hsl 2012-06-14
		 * @param uuid 黑名单信息的UUID
		 * 
		 * @return SamApiBaseResult 执行的结果
		 * */
		public SamApiBaseResult deleteInhibit(String uuid);
		//modified by hsl 2012-06-14
		public SendMessageByDeviceResult sendMessageByDevice(String[] deviceIps,String noticeContent,Date noticeExpireTtime);
		//返回全部模板和套餐信息 ok
		public FindAllUserTemplatePackagesResult findAllUserTemplatePackages();
		//返回全部用户组信息，包括层次关系 ok
		public FindAllUserGroupsResult findAllUserGroups();
		//查询BACL，返回系统中所有BACL名称 ok
		public FindAllBaclNamesResult findAllBaclNames();
		//欠费透支账户查询1：欠费用户，2：透支用户
		public FindAbnormalUserinfosResult findAbnormalUserinfos(int type);
		//修改用户绑定信息 ok
		//modified by hsl 2012-06-14
		public SamApiBaseResult updateUserBindInfo(SpiUserBindInfo spiUserBindInfo);
		public SamApiBaseResult updateUserBindInfoV2(SpiUserBindInfoV2 spiUserBindInfoV2);
		//根据用户名查询用户全部绑定信息 ok
		public FindUserBindInfoByUserIdResult findUserBindInfoByUserId(String userid);
		public FindUserBindInfoByUserIdResultV2 findUserBindInfoByUserIdV2(String userid);
		//根据管理员ID查询管理员详细信息ok
		public FindManagerInfoByIdResult findManagerInfoById(String userId);
		//验证管理员密码 ok
		public VerifyManagerPasswordResult verifyManagerPassword(String userid,String password);
		//	返回所有用户自定义字段ok
		public FindAllCustomFieldResult findAllCustomFieldForUserinfo();
		//返回系统中定义的所有计费策略名称 ok
		public FindAllPoliciesResult findAllPolicies();
		//发送实时或离线消息ok
		//参数：用户名，消息内容，消息类型 0 实时消息 1是离线消息
		public SendMessageResult sendMessage(String userId,String msgContent,int msgType);
		
		/**
		 * @param userId
		 * @param msgContent
		 * @return
		 */
		public SendMessageResult sendMessageV2(String userId,String msgContent,int msgType);
		//获取流量信息
		public FindFlowDescInfoResult findFlowDescInfo(String userId);
		/**
		 * 数据导出接口，
		 * @param ftp配置信息、要导出的表的名称
		 * @return 能否定时导出成功,导出工作一般放到午夜2：00以后工作
		 */
		public SamApiBaseResult saveFtpExport(String ftpServerIp,Long ftpServerPort,String ftpBakPath,String ftpUser,String ftpPass,String exportTable[]);
		public FindFtpExportResult findFtpExport();
		public String createTxid();
	
		public EncryptPasswordResult encryptPassword (String password);
		public ResumeUserResult resumeUser(String userId);
		public QueryLoginFailLogResult queryLoginFailLog(QueryLoginFailLogParams params);
		public QueryOnlineDetailResult queryOnlineDetail(QueryOnlineDetailParams params);
		public QueryOnlineDetailResultV2 queryOnlineDetailV2(QueryOnlineDetailParams params);
		public AccountOperationResult fund(String userId,int fundType,BigDecimal fee,String txid);
		public AccountOperationResult refund(String userId, int refundType, BigDecimal fee, String txid);
		public AccountOperationResult preAccountFeeTransfer(String userId,BigDecimal fee,String txid);
		public AccountOperationResult ChargeCard(String userId,String chargeCardType,String cardNo,String password,String txid);
		public FindChargeCardResult findChargeChardInfo(String cardNo,String password);
		public QueryTxidResult findExistTxCommand(String txid);
		
		/*
		 * bankFund 保存网上营业厅的缴费记录
		 * 
		 * @param userId 用户名		
		 * 
		 * @param fundType 缴费类型 	
		 * 	 
		 * @param fee  缴费金额		 
		 * 
		 * @param txid  业务操作唯一标示
		 * 
		 * @param tgTradeNO  订单编号
		 * 
		 *  @param tradeNo   交易流水号
		 *  
		 * @return SuspendUserResult 网上营业厅缴费返回的对象结果
		 * */
		
		/*
		 * queryAccountFlow 是根据订单号或交易流水号查询缴费业务
		 * 
		 * @param userId 用户名
		 * 
		 * @param rgTradeNo  订单号
		 * 
		 * @param tradeNo  交易号
		 * 
		 * @param fromDate 开始时间
		 * 
		 * @param toDate 结束时间
		 * 
		 * @param chargeType 计费类型 -缴费
		 * 
		 * @param offSet 记录起始行
		 * 
		 * @param limit 页大小
		 * 
		 * @return QueryAccountFlowResult 查账务流水返回的对象结果
		 * */
		public  QueryBankAccountFlowResult queryBankAccountFlow(QueryBankAccountFlowParams params);
		
		public CompensatePeriodBillResult compensatePeriodBill (String userId,int type,BigDecimal value);

		/*
		 * 查询用户在线状态
		 * @userId 用户名
		 * */
		
		public FindOnlineStateResult findOnlineState (String userId);
		
		/*
		 * 查询用户在线列表
		 * @FindOnlineUserListResult 返回用户ID
			 * */
		
//		public  FindOnlineUserIdListResult findOnlineUserIdList ();	
		
		
		public  SamApiBaseResult suspendUser(String userId);
		
		/*
		 * verifyPassword 是根据用户名和密码验证用户密码
		 * 
		 * @param userId 用户名
		 * 
		 * @param password 密码
		 * 
		 * @return VerifyPasswordResult 是验证用户密码返回的对象结果
		 * */
		public  SamApiBaseResult verifyPassword(String userId,String password);
		
		/*
		 * kickOnlineUser 是根据用户名强制用户下线
		 * 
		 * @param userId 用户名
		 * 
		 * @param blackListFlag 是否加入黑名单的标记
		 * 
		 * @param blackListExpireTime 是黑名单的的有效期
		 * 
		 * @param blackListMessage 是黑名单的的提示信息
		 * 
		 * @return KickOnlineUserResult 是强制用户下线返回的对象结果
		 * */
		public  SamApiBaseResult kickOnlineUser(String userId,int blackListFlag,int blackListExpireTime,String blackListMessage);
		
		/*
		 * querySelfServPerm 是查询用户的自助权限
		 * 
		 * @return QuerySelfServPermResult 是查询用户自助权限返回的对象结果
		 * */
		public  FindSelfServPermResult findSelfServPerm();

		/*
		 * queryNtdFlow 是根据用户名查询网管流量
		 * 
		 * @param userId 用户名
		 * 
		 * @param userIp 用户IP
		 * 
		 * @param fromDate 开始时间
		 * 
		 * @param toDate 结束时间
		 * 
		 * @param offSet 记录起始行
		 * 
		 * @param limit 页大小
		 * 
		 * @return QueryNtdFlowResult 是查询网管流量返回的对象结果
		 * */
		public QueryNtdFlowResult queryNtdFlow(QueryNtdFlowParams params);
		
		/*
		 * queryAccountFlow 是根据用户名查询网管流量
		 * 
		 * @param userId 用户名
		 * 
		 * @param fromDate 开始时间
		 * 
		 * @param toDate 结束时间
		 * 
		 * @param chargeType 计费类型 
		 * 
		 * @param offSet 记录起始行
		 * 
		 * @param limit 页大小
		 * 
		 * @return QueryAccountFlowResult 是查账务流水返回的对象结果
		 * */
		public  QueryAccountFlowResult queryAccountFlow(QueryAccountFlowParams params);
		/*
		 * queryUser 是根据用户名查询用户信息
		 * 
		 * @param userId 用户名
		 * 
		 * 
		 * @return QueryUserResult 是查账务流水返回的对象结果
		 * */
		public  QueryAccountFlowResult queryAccountFlowAll(QueryAccountFlowAllParams params);
		/*
		 * queryUser 是根据用户名查询用户信息
		 * 
		 * @param userId 用户名
		 * 
		 * 
		 * @return QueryUserResult 是查账务流水返回的对象结果
		 * */
		public  QueryUserResult queryUser(QueryUserParams params);
		
		/*
		 * delUser 是根据用户名进行销户
		 * 
		 * @param userId 用户名
		 * 
		 * @return WebserviceBaseResult 是销户返回的对象结果
		 * */
		public  SamApiBaseResult  delUser(String userId);
		
		/*
		 * logicDelUser 是根据用户名进行预销户
		 * 
		 * @param userId 用户名
		 * 
		 * @return WebserviceBaseResult 是预销户返回的对象结果
		 * */
		public  SamApiBaseResult logicDelUser(String userId);

		/*
		 * addUser 是用户开户
		 * 
		 * @param userId 用户名
		 * 
		 * @return WebserviceBaseResult 是预销户返回的对象结果
		 * */
		public  SamApiBaseResult addUserAndBindOperatorUserInfo(AddUserAndBindOperatorUserInfoParams params);
		/*
		 * addUser 是用户开户
		 * 
		 * @param userId 用户名
		 * 
		 * @return WebserviceBaseResult 是预销户返回的对象结果
		 * */
		public  SamApiBaseResult addUser(AddUserParams params);
		
		/*
		 * regUser 是用户开户
		 * 
		 * @param userId 用户名
		 * 
		 * @return WebserviceBaseResult 是预销户返回的对象结果
		 * */
		public  SamApiBaseResult regUser(RegUserParams params);
		
		/*
		 * modifyPassword 是根据用户名和密码修改 用户密码
		 * 
		 * @param userId 用户名
		 * 
		 * @param password 密码
		 * 
		 * @param onlineHandleType 用户在线处理类型，0不修改，1踢下线并修改，2修改但不踢下线
		 * 
		 * @return WebserviceBaseResult 是修改用户密码返回的对象结果
		 * */
		public SamApiBaseResult modifyPassword(String userId ,String password);
		
		/*
		 * modifyUser 是根据用户名和密码修改 用户密码
		 * 
		 * @param userId 用户名
		 * 
		 * @param password 密码
		 * 
		 * @return WebserviceBaseResult 是修改用户密码返回的对象结果
		 * */
		public SamApiBaseResult modifyUser(ModifyUserParams params);
		
		/*
		 * modifyUserPackage 是变更用户 套餐
		 * 
		 * @param userId 用户名
		 * 
		 * @param packageId 套餐名
		 * 
		 * @param enableType 生效类型，0立即生效，1下周其生效，2永不生效
		 * 
		 * @param onlineHandleType 用户在线处理类型，0不修改，1踢下线并修改，2修改但不踢下线
		 * 
		 * @return WebserviceBaseResult 是修改用户密码返回的对象结果
		 * */
		public SamApiBaseResult modifyUserPackage(String userId,String atName,String packageId,int enableType);
		
		/*
		 * modifyUserGroup 是变更用户 套餐
		 * 
		 * @param userId 用户名
		 * 
		 * @param groupId 组名
		 * 
		 * @param onlineHandleType 用户在线处理类型，0不修改，1踢下线并修改，2修改但不踢下线
		 * 
		 * @return WebserviceBaseResult 是修改用户密码返回的对象结果
		 * */
		public SamApiBaseResult modifyUserGroup(String userId,String groupId);
		
		/*
		 * 添加用户绑定信息
		 * added by hsl 2012-06-14
		 * @param userId 用户名
		 * @param spiUserBindInfo 需要添加的绑定信息
		 * 
		 * @return SamApiBaseResult 执行的结果
		 * */
		public  SamApiBaseResult addUserBindInfo(String userid,SpiUserBindInfo spiUserBindInfo);
		public  SamApiBaseResult addUserBindInfoV2(String userid,SpiUserBindInfoV2 spiUserBindInfo);
		/*
		 * 添加用户绑定信息
		 * added by hsl 2012-06-14
		 * @param userBindInfoUuid 绑定信息的UUID
		 * 
		 * @return SamApiBaseResult 执行的结果
		 * */
		public  SamApiBaseResult deleteUserBindInfo(String userBindInfoUuid);
		/*
		 * 查询黑名单
		 * added by hsl 2012-06-14
		 * @params 查询条件
		 * 
		 * @return QueryInhibitResult 执行的结果
		 * */
		public QueryInhibitResult queryInhibit(QueryInhibitParam params);
		
		
		public QueryInhibitResultV2 queryInhibitV2(QueryInhibitParamV2 params);
		
		/*
		 * 将逻辑销户转成正常用户
		 * added by hsl 2012-06-15
		 * @userId 用户ID
		 * 
		 * @return SamApiBaseResult 执行的结果
		 * */
		public SamApiBaseResult reLogicDelUser(String userId);
		
		/*
		 * 查询在线人数
		 * added by hsl 2012-06-15
		 * @userId 用户ID
		 * 
		 * @return SamApiBaseResult 执行的结果
		 * */
		public FindOnlineUserCountResult findOnlineUserCount();
		/*
		 * 查询在线人数
		 * added by hsl 2012-06-15
		 * @userId 用户ID
		 * @param QueryOnlineUserParam查询在线用户条件
		 * @return QueryOnlineUserResult 执行的结果
		 * */
		public QueryOnlineUserResult queryOnlineUser(QueryOnlineUserParam param);
		public QueryOnlineUserResultV2 queryOnlineUserV2(QueryOnlineUserParam param);
		
		public SamApiBaseResult deleteOnlineUser(String userId);
		
		/** 绑定运营商账号
		 * @param userId 用户名
		 * @param operatorName 运营商名称
		 * @param operatorUserId 运营商账号
		 * @param operatorUserPassword 运营商密码
		 * @return
		 */
//		此接口用于绑定运营商，不包含SAM中手机号码前缀校验、自助端绑定次数限制等功能。如若需要
		public SamApiBaseResult bindOperator(String userId,String operatorName, String operatorUserId,String operatorUserPassword);
		
		/**解绑运营商账号
		 * @param userId 用户名
		 * @return
		 */
		public SamApiBaseResult unbindOperator(String userId);
		
		/** 更改绑定运营商账号
		 * @param userId 用户名
		 * @param operatorName 运营商名称
		 * @param operatorUserId 运营商账号
		 * @param operatorUserPassword 运营商密码
		 * @return
		 */
		public SamApiBaseResult rebindOperator(String userId,String operatorName, String operatorUserId,String operatorUserPassword);
		
		/**
		 * 查询报修信息
		 */
		public QueryRepairReportResult queryRepairReport(QueryRepairReportParam params);
		
		//added by hsl 2012-06-08 查询系统当前是否处于逃生状态
		public QueryBypassStateResult queryBypassState();
		
		public AccountOperationResult fundWebFee(String string, BigDecimal bigDecimal, int i, String createTxid);
		
		
		/*
		 * rebindOperatorV2 更改绑定运营商账号
		 * 
		 * @param userId 用户名
		 * 
		 * @param operatorName 运营商名称
		 * 
		 * @param operatorUserId 运营商账号
		 * 
		 * @param operatorUserPassword 运营商密码
		 * 
		 * @return ReBindOperatorForSelfResult 是更改绑定运营商账号返回的对象结果
		 * */
		/*
		 针对 rebindOperator接口，增加手机前缀校验、运营商账号更改次数限制等功能
		 对由此需求的用户较为合适
		 * */
		public ReBindOperatorForSelfResult rebindOperatorV2(String userId,String operatorName, String operatorUserId,String operatorUserPassword);
		
		
		/*
		 * bindOperatorV2 绑定运营商账号
		 * 
		 * @param userId 用户名
		 * 
		 * @param operatorName 运营商名称
		 * 
		 * @param operatorUserId 运营商账号
		 * 
		 * @param operatorUserPassword 运营商密码
		 * 
		 * @return WebserviceBaseResult 是绑定运营商账号返回的对象结果
		 * */
		/*
		 针对 bindOperator接口，增加手机前缀校验、运营商账号绑定次数限制等功能
		 对由此需求的用户较为合适	  
		 	
		 * */
		public BindOperatorForSelfResult bindOperatorV2(String userId,String operatorName, String operatorUserId,String operatorUserPassword);
		
		
		
//		---------------------------------------PhoneSelfInterface--------------------------------------------------------------------------------------
		
//		-------------------------------------------------------Start--------------------------------------------------------------------------------------
		
		/* 以下接口仅给手机自助端使用，如果其他调用，请咨询相关开发人员；如若添加其他接口，请添加在PhoneSelfInterface虚线以上*/
		
		/*
		 * 查询用户是否使用周期计费策略
		 * added by dzf 2013-05-29
		 * @userId 用户ID
		 * @return isPeriodStrategyUserResult 执行的结果
		 * */
		public VerifyPeriodStrategyUserResult verifyPeriodStrategyUser(String userID);
		
		/*
		 * 查询变更套餐详细记录
		 * added by dzf 2013-05-29
		 * @userId 用户ID
		 * @userId查询变更用户条件
		 * @return QueryUserPolicyInfoChangeResult 执行的结果
		 * */
		public QueryUserPolicyInfoChangeResult queryUserPolicyInfoChange(String userId);
		/*
		 * 本地账户与运营商账户统一认证
		 * added by dzf 2013-06-29
		 * @userId 用户名
		 * @password 用户密码
		 * */
		public VerifyLoginByAccountTypeResult verifyLoginByAccountType(String userId,String password);
		
		
		/*
		 * 套餐变更条件判断
		 * added by dzf 2013-06-29
		 * @userId 用户名
		 * */
		public SamApiBaseResult  changePackage4Self(String userId,String packageId,String changeType);
		
		
		/*
		 * 用户恢复权限判断
		 * added by dzf 2013-06-29
		 * @userId 用户名
		 * */
		public SamApiBaseResult  resume4SelfPermission(String userId);
		
		/*
		 * 用户暂停权限判断
		 * added by dzf 2013-06-29
		 * @userId 用户名
		 * */
		public SamApiBaseResult  suspend4SelfPermission(String userId);
		
		/*
		 * modifyUserPackage 变更用户套餐
		 * 
		 * @param userId 用户名
		 * 
		 * @param packageId 套餐名
		 * 
		 * @param enableType 生效类型，0立即生效，1下周其生效，2永不生效
		 * 
		 * @return WebserviceBaseResult 是变更用户 套返回的对象结果
		 * */
		public SamApiBaseResult modifyUserPackageForSelf(String userId,String atName,String packageId,int enableType);
		
		/*
		 * bindOperatorForSelf 绑定运营商账号
		 * 
		 * @param userId 用户名
		 * 
		 * @param operatorName 运营商名称
		 * 
		 * @param operatorUserId 运营商账号
		 * 
		 * @param operatorUserPassword 运营商密码
		 * 
		 * @return WebserviceBaseResult 是绑定运营商账号返回的对象结果
		 * */
		public BindOperatorForSelfResult bindOperatorForSelf(String userId,String operatorName, String operatorUserId,String operatorUserPassword);
		
		/*
		 * unbindOperatorForSelf 解绑运营商账号
		 * 
		 * @param userId 用户名
		 * 
		 * */
		public SamApiBaseResult unbindOperatorForSelf(String userId);
		
		/*
		 * rebindOperatorForSlef 更改绑定运营商账号
		 * 
		 * @param userId 用户名
		 * 
		 * @param operatorName 运营商名称
		 * 
		 * @param operatorUserId 运营商账号
		 * 
		 * @param operatorUserPassword 运营商密码
		 * 
		 * @return ReBindOperatorForSelfResult 是更改绑定运营商账号返回的对象结果
		 * */
		public ReBindOperatorForSelfResult rebindOperatorForSlef(String userId,String operatorName, String operatorUserId,String operatorUserPassword);
		/*
		 * chargeCardForSelf 自助端充值卡充值
		 * 
		 * @param userId 用户名
		 * 
		 * @param chargeCardType 卡类型
		 * 
		 * @param cardNo 充值卡号
		 * 
		 * @param password 充值卡密码
		 * 
		 * @param  txid 
		 * 
		 * @return AccountOperationResult 是自助端充值卡充值返回的对象结果
		 * */
		
		public AccountOperationResult chargeCardForSelf(String userId,String chargeCardType,String cardNo,String password,String txid);
		
		
		/*
		 * chargeCardForSelf 自助端充值卡充值
		 * 
		 * @param userId 用户名
		 * 
		 * @param chargeCardType 卡类型
		 * 
		 * @param cardNo 充值卡号
		 * 
		 * @param password 充值卡密码
		 * 
		 * @param  txid 
		 * 
		 * @return AccountOperationResult 是自助端充值卡充值返回的对象结果
		 * */
		
		public ModifyPackageConfirmInfosResult modifyPackageConfirmInfos(String userId, String templateName, String userPackageName, short operationTimeType);
		
		/*
		 * unbindOperatorForSelf 解绑运营商账号
		 * 
		 * @param userId 用户名
		 * 
		 * */
		public QueryPermissionListForPhoneSelfResult queryPermissionListForPhoneSelf(String userId);
		
//		-------------------------------------------------------End --------------------------------------------------------------------------------------
//		---------------------------------------PhoneSelfInterface--------------------------------------------------------------------------------------
		
		
		
}
