package com.ruijie.spl.api;

/**
 *根据设备Ip列表发送消息的返回结果
 * @author 黄树林
 * @version 1.0
 * @since 1.0
 * @date: 2012-06-13
 */

public class SendMessageByDeviceResult extends SamApiBaseResult {
	private String [] failDeviceIps;//消息发送失败设备的Ip数组

	public SendMessageByDeviceResult(){
		
	}
	
	public SendMessageByDeviceResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.failDeviceIps=null;
	}
	
	public SendMessageByDeviceResult(int errorCode,String errorMessage,String[] failDeviceIps){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.failDeviceIps=failDeviceIps;
	}
	
	public String[] getFailDeviceIps() {
		return failDeviceIps;
	}
	public void setFailDeviceIps(String[] failDeviceIps) {
		this.failDeviceIps = failDeviceIps;
	}
}
