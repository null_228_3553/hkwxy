package com.ruijie.spl.api;

public class FindUserBindInfoByUserIdResult extends SamApiBaseResult{
private SpiUserBindInfo[] infos;

public FindUserBindInfoByUserIdResult(){
	
}

public FindUserBindInfoByUserIdResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setInfos(null);
}

public FindUserBindInfoByUserIdResult(int errorCode,String errorMessage,SpiUserBindInfo[] infos){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setInfos(infos);
}

public SpiUserBindInfo[] getInfos() {
	return infos;
}

public void setInfos(SpiUserBindInfo[] infos) {
	this.infos = infos;
}
}
