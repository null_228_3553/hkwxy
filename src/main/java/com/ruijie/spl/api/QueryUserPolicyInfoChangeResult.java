package com.ruijie.spl.api;

import java.util.List;

public class QueryUserPolicyInfoChangeResult extends SamApiBaseResult{
private List<UserPolicyChangeName> userPolicyChange;
int total;

public QueryUserPolicyInfoChangeResult(){
	
}

public QueryUserPolicyInfoChangeResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setUserPolicyChange(null);
	this.setTotal(0);
}

public QueryUserPolicyInfoChangeResult(int errorCode,String errorMessage,List<UserPolicyChangeName> userPolicyChange,int total){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setUserPolicyChange(userPolicyChange);
	this.setTotal(total);
}

public int getTotal() {
	return total;
}

public void setTotal(int total) {
	this.total = total;
}

public void setUserPolicyChange(List<UserPolicyChangeName> userPolicyChange) {
	this.userPolicyChange = userPolicyChange;
}

public List<UserPolicyChangeName> getUserPolicyChange() {
	return userPolicyChange;
}



}