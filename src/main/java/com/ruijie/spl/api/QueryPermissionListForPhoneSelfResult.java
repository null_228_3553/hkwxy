package com.ruijie.spl.api;

public class QueryPermissionListForPhoneSelfResult extends SamApiBaseResult{
	private String [] permissionInfos;
	public QueryPermissionListForPhoneSelfResult(){
		
	}
	public QueryPermissionListForPhoneSelfResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}

	public QueryPermissionListForPhoneSelfResult(int errorCode,String errorMessage,String []  permissionInfos){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.permissionInfos=permissionInfos;
	}
	public String[] getPermissionInfos() {
		return permissionInfos;
	}
	public void setPermissionInfos(String[] permissionInfos) {
		this.permissionInfos = permissionInfos;
	}

}