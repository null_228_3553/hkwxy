package com.ruijie.spl.api;

public class UserTemplatePackageInfo {
private String UserPackageId;
private String UserTemplateId;
public UserTemplatePackageInfo(){
	
}
public UserTemplatePackageInfo(String UserTemplateId,String UserPackageId){
	this.UserPackageId=UserPackageId;
	this.UserTemplateId=UserTemplateId;
}
public String getUserPackageId() {
	return UserPackageId;
}
public void setUserPackageId(String userPackageId) {
	UserPackageId = userPackageId;
}
public String getUserTemplateId() {
	return UserTemplateId;
}
public void setUserTemplateId(String userTemplateId) {
	UserTemplateId = userTemplateId;
}
}
