package com.ruijie.spl.api;

import java.util.Date;

/**
 * 查询系统逃生状态的返回值
 * @author 黄树林
 * @version 1.0
 * @since 1.0
 * @date: 2012-06-8
 */

public class QueryBypassStateResult extends SamApiBaseResult {
	private boolean isOpenBypass=false;//是否逃生，true当前处于逃生，false当前为逃生
	private Date startDate=null;//如果当前系统正处于逃生，则存储当前逃生的时间
	
	public QueryBypassStateResult(){
		
	}
	public QueryBypassStateResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	public QueryBypassStateResult(int errorCode,String errorMessage,boolean isOpenBypass,Date startDate){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.setOpenBypass(isOpenBypass);
		this.startDate=startDate;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public boolean isOpenBypass() {
		return isOpenBypass;
	}
	public void setOpenBypass(boolean isOpenBypass) {
		this.isOpenBypass = isOpenBypass;
	}

}
