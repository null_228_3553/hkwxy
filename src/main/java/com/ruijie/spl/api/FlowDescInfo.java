package com.ruijie.spl.api;

import java.io.Serializable;
public class FlowDescInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	private String usedFreeDesc;//已用的免费量/总免费量
	private String chargeDesc;//已计费的总业务量	
	public String getUsedFreeDesc() {
		return usedFreeDesc;
	}
	public void setUsedFreeDesc(String usedFreeDesc) {
		this.usedFreeDesc = usedFreeDesc;
	}
	public String getChargeDesc() {
		return chargeDesc;
	}
	public void setChargeDesc(String chargeDesc) {
		this.chargeDesc = chargeDesc;
	}
}

