package com.ruijie.spl.api;


public class ReBindOperatorForSelfResult extends SamApiBaseResult{
     private int reBindLeftTimes;
     
     public ReBindOperatorForSelfResult(){
    	 
     }
     
     public  ReBindOperatorForSelfResult(int errorCode, String errorMessage){
 			this.errorCode = errorCode;
 			this.errorMessage = errorMessage;
     }
     
     
     public  ReBindOperatorForSelfResult(int errorCode, String errorMessage,int bindTimes){
    	 	this.errorCode = errorCode;
  			this.errorMessage = errorMessage;
    		this.reBindLeftTimes = bindTimes;    	 
     }

	public int getBindTimes() {
		return reBindLeftTimes;
	}

	public void setBindTimes(int bindTimes) {
		this.reBindLeftTimes = bindTimes;
	}
     
     
}