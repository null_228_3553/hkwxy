package com.ruijie.spl.api;

public class QueryTxidResult extends SamApiBaseResult{
	private boolean isSuccess;
	public QueryTxidResult(){
		
	}
	
	public QueryTxidResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	
	public QueryTxidResult(int errorCode,String errorMessage,boolean isSuccess){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.isSuccess=isSuccess;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean success) {
		this.isSuccess = success;
	}

}
