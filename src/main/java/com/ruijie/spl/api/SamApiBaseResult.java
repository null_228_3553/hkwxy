package com.ruijie.spl.api;

public class SamApiBaseResult {
	protected String errorMessage ;
	protected int errorCode;
	public SamApiBaseResult(){
		
	}
	public SamApiBaseResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
