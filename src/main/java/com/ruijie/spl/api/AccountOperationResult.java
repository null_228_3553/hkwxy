package com.ruijie.spl.api;

public class AccountOperationResult extends SamApiBaseResult{
	AccountingInfo accountInfo;
	public AccountOperationResult(){
		
	}
	
	public AccountOperationResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.accountInfo=null;
	}
	
	public AccountOperationResult(int errorCode,String errorMessage,AccountingInfo accountInfo){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.accountInfo=accountInfo;
	}
	public AccountingInfo getAccountInfo() {
		return accountInfo;
	}
	public void setAccountInfo(AccountingInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

}
