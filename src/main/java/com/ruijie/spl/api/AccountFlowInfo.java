package com.ruijie.spl.api;

import java.math.BigDecimal;
import java.util.Date;

public class AccountFlowInfo  {
	private String userId;
	private String serviceId;
	private int chargeType;
	private int accessType;
	private Date beginTime;
	private Date createTime;
	private boolean isBillOut;
	private boolean isWriteOff;
	private Date destroyTime;
	private int destroyType;
	private BigDecimal fee;
	private BigDecimal badBillFee;
	private BigDecimal oddFee;
	private BigDecimal accountFee;
	private BigDecimal accountPreFee;
	private long sesstionTime;
	private BigDecimal totalTrafficByte;
	private BigDecimal inlandUpFlowByte;
	private BigDecimal inlandDownFlowByte;
	private BigDecimal foreignUpFlowByte;
	private BigDecimal foreignDownFlowByte;
	private BigDecimal campusUpFlowByte;
	private BigDecimal campusDownFlowByte;
	private BigDecimal totalBillFlowByte;
	private String ntdFlowCause;
	private long dayComps;
	private long monthComps;
	private long  timeComps;
	private BigDecimal trafficComps;
	private BigDecimal inlandUpComps;
	private BigDecimal inlandDownComps;
	private BigDecimal foreUpComps;
	private BigDecimal foreDownComps;
	private BigDecimal flowSumComps;
	private String packageName;
	private String serviceSuffix;
	private String areaName;
	private int accounttingRule;
	private String policyId;
	private int webOpType;
	private String managerIp;
	private String managerId;
	private BigDecimal overRaraftFeeLeft;
	private BigDecimal overRaraftFee;
	private String groupId;
	private String reserved0;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	private String reserved4;
	private String reserved5;
	private String reserved6;
	private String reserved7;
	private String reserved8;
	private String reserved9;

	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
//	public String getAccountID() {
//		return this.accountID;
//	}
//	public void setAccountID(String accountID) {
//		this.accountID = accountID;
//	}
	public String getServiceId() {
		return this.serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public int getChargeType() {
		return this.chargeType;
	}
	public void setChargeType(int chargeType) {
		this.chargeType = chargeType;
	}
	public int getAccessType() {
		return this.accessType;
	}
	public void setAccessType(int accessType) {
		this.accessType = accessType;
	}
	public Date getBeginTime() {
		return this.beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getCreateTime() {
		return this.createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public boolean isBillOut() {
		return this.isBillOut;
	}
	public void setBillOut(boolean isBillOut) {
		this.isBillOut = isBillOut;
	}
	public boolean isWriteOff() {
		return this.isWriteOff;
	}
	public void setWriteOff(boolean isWriteOff) {
		this.isWriteOff = isWriteOff;
	}
	public Date getDestroyTime() {
		return this.destroyTime;
	}
	public void setDestroyTime(Date destroyTime) {
		this.destroyTime = destroyTime;
	}
	public int getDestroyType() {
		return this.destroyType;
	}
	public void setDestroyType(int destroyType) {
		this.destroyType = destroyType;
	}
	public BigDecimal getFee() {
		return this.fee;
	}
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	public BigDecimal getBadBillFee() {
		return this.badBillFee;
	}
	public void setBadBillFee(BigDecimal badBillFee) {
		this.badBillFee = badBillFee;
	}
	public BigDecimal getOddFee() {
		return this.oddFee;
	}
	public void setOddFee(BigDecimal oddFee) {
		this.oddFee = oddFee;
	}
	public BigDecimal getAccountFee() {
		return this.accountFee;
	}
	public void setAccountFee(BigDecimal accountFee) {
		this.accountFee = accountFee;
	}
	public BigDecimal getAccountPreFee() {
		return this.accountPreFee;
	}
	public void setAccountPreFee(BigDecimal accountPreFee) {
		this.accountPreFee = accountPreFee;
	}
	public long getSesstionTime() {
		return this.sesstionTime;
	}
	public void setSesstionTime(long sesstionTime) {
		this.sesstionTime = sesstionTime;
	}
	public BigDecimal getTotalTrafficByte() {
		return this.totalTrafficByte;
	}
	public void setTotalTrafficByte(BigDecimal totalTrafficByte) {
		this.totalTrafficByte = totalTrafficByte;
	}
	public BigDecimal getInlandUpFlowByte() {
		return this.inlandUpFlowByte;
	}
	public void setInlandUpFlowByte(BigDecimal inlandUpFlowByte) {
		this.inlandUpFlowByte = inlandUpFlowByte;
	}
	public BigDecimal getInlandDownFlowByte() {
		return this.inlandDownFlowByte;
	}
	public void setInlandDownFlowByte(BigDecimal inlandDownFlowByte) {
		this.inlandDownFlowByte = inlandDownFlowByte;
	}
	public BigDecimal getForeignUpFlowByte() {
		return this.foreignUpFlowByte;
	}
	public void setForeignUpFlowByte(BigDecimal foreignUpFlowByte) {
		this.foreignUpFlowByte = foreignUpFlowByte;
	}
	public BigDecimal getForeignDownFlowByte() {
		return this.foreignDownFlowByte;
	}
	public void setForeignDownFlowByte(BigDecimal foreignDownFlowByte) {
		this.foreignDownFlowByte = foreignDownFlowByte;
	}
	public BigDecimal getCampusUpFlowByte() {
		return this.campusUpFlowByte;
	}
	public void setCampusUpFlowByte(BigDecimal campusUpFlowByte) {
		this.campusUpFlowByte = campusUpFlowByte;
	}
	public BigDecimal getCampusDownFlowByte() {
		return this.campusDownFlowByte;
	}
	public void setCampusDownFlowByte(BigDecimal campusDownFlowByte) {
		this.campusDownFlowByte = campusDownFlowByte;
	}
	public BigDecimal getTotalBillFlowByte() {
		return this.totalBillFlowByte;
	}
	public void setTotalBillFlowByte(BigDecimal totalBillFlowByte) {
		this.totalBillFlowByte = totalBillFlowByte;
	}
	public String getNtdFlowCause() {
		return this.ntdFlowCause;
	}
	public void setNtdFlowCause(String ntdFlowCause) {
		this.ntdFlowCause = ntdFlowCause;
	}
	public long getDayComps() {
		return this.dayComps;
	}
	public void setDayComps(long dayComps) {
		this.dayComps = dayComps;
	}
	public long getMonthComps() {
		return this.monthComps;
	}
	public void setMonthComps(long monthComps) {
		this.monthComps = monthComps;
	}
	public long getTimeComps() {
		return this.timeComps;
	}
	public void setTimeComps(long timeComps) {
		this.timeComps = timeComps;
	}
	public BigDecimal getTrafficComps() {
		return this.trafficComps;
	}
	public void setTrafficComps(BigDecimal trafficComps) {
		this.trafficComps = trafficComps;
	}
	public BigDecimal getInlandUpComps() {
		return this.inlandUpComps;
	}
	public void setInlandUpComps(BigDecimal inlandUpComps) {
		this.inlandUpComps = inlandUpComps;
	}
	public BigDecimal getInlandDownComps() {
		return this.inlandDownComps;
	}
	public void setInlandDownComps(BigDecimal inlandDownComps) {
		this.inlandDownComps = inlandDownComps;
	}
	public BigDecimal getForeUpComps() {
		return this.foreUpComps;
	}
	public void setForeUpComps(BigDecimal foreUpComps) {
		this.foreUpComps = foreUpComps;
	}
	public BigDecimal getForeDownComps() {
		return this.foreDownComps;
	}
	public void setForeDownComps(BigDecimal foreDownComps) {
		this.foreDownComps = foreDownComps;
	}
	public BigDecimal getFlowSumComps() {
		return this.flowSumComps;
	}
	public void setFlowSumComps(BigDecimal flowSumComps) {
		this.flowSumComps = flowSumComps;
	}
	public String getPackageName() {
		return this.packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getServiceSuffix() {
		return this.serviceSuffix;
	}
	public void setServiceSuffix(String serviceSuffix) {
		this.serviceSuffix = serviceSuffix;
	}
	public String getAreaName() {
		return this.areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public int getAccounttingRule() {
		return this.accounttingRule;
	}
	public void setAccounttingRule(int accounttingRule) {
		this.accounttingRule = accounttingRule;
	}
	public String getGroupId() {
		return this.groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getPolicyId() {
		return this.policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public int getWebOpType() {
		return this.webOpType;
	}
	public void setWebOpType(int webOpType) {
		this.webOpType = webOpType;
	}
	public String getManagerIp() {
		return this.managerIp;
	}
	public void setManagerIp(String managerIp) {
		this.managerIp = managerIp;
	}
	public String getManagerId() {
		return this.managerId;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	public BigDecimal getOverRaraftFeeLeft() {
		return this.overRaraftFeeLeft;
	}
	public void setOverRaraftFeeLeft(BigDecimal overRaraftFeeLeft) {
		this.overRaraftFeeLeft = overRaraftFeeLeft;
	}
	public BigDecimal getOverRaraftFee() {
		return this.overRaraftFee;
	}
	public void setOverRaraftFee(BigDecimal overRaraftFee) {
		this.overRaraftFee = overRaraftFee;
	}
	public String getReserved0() {
		return reserved0;
	}
	public void setReserved0(String reserved0) {
		this.reserved0 = reserved0;
	}
	public String getReserved1() {
		return reserved1;
	}
	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}
	public String getReserved2() {
		return reserved2;
	}
	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}
	public String getReserved3() {
		return reserved3;
	}
	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}
	public String getReserved4() {
		return reserved4;
	}
	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}
	public String getReserved5() {
		return reserved5;
	}
	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}
	public String getReserved6() {
		return reserved6;
	}
	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}
	public String getReserved7() {
		return reserved7;
	}
	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}
	public String getReserved8() {
		return reserved8;
	}
	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}
	public String getReserved9() {
		return reserved9;
	}
	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}

	
}
