package com.ruijie.spl.api;

public class QueryNtdFlowResult extends SamApiBaseResult {
	private NtdFlowInfo [] data;
	private int total;
	
	public QueryNtdFlowResult(){
		
	}
	
	public QueryNtdFlowResult(int errorCode,String message){
		this.errorCode=errorCode;
		this.errorMessage=message;
	}
	
	public NtdFlowInfo[] getData() {
		return data;
	}
	public void setData(NtdFlowInfo[] data) {
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
