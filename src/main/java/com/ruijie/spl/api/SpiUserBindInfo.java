package com.ruijie.spl.api;

public class SpiUserBindInfo {
	private String userBindInfoUuid;
	private Short    accessType;
	private String userIpv4;
	private String userMac;
	private String nasIpv4;
	private Long   nasPort;
	private String wpNasIp;
	private Long    wpNasPort;
	private String apMac;
	private String ssid;
	private String nasIpv6;
	private String userIpv6;
	private String userInfoUuid;
	private Integer  listDefaultShow;
	
	public String getUserBindInfoUuid() {
		return userBindInfoUuid;
	}
	public void setUserBindInfoUuid(String userBindInfoUuid) {
		this.userBindInfoUuid = userBindInfoUuid;
	}
	public Short getAccessType() {
		return accessType;
	}
	public void setAccessType(Short accessType) {
		this.accessType = accessType;
	}
	public String getUserIpv4() {
		return userIpv4;
	}
	public void setUserIpv4(String userIpv4) {
		this.userIpv4 = userIpv4;
	}
	public String getUserMac() {
		return userMac;
	}
	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}
	public String getNasIpv4() {
		return nasIpv4;
	}
	public void setNasIpv4(String nasIpv4) {
		this.nasIpv4 = nasIpv4;
	}
	public Long getNasPort() {
		return nasPort;
	}
	public void setNasPort(Long nasPort) {
		this.nasPort = nasPort;
	}
	public String getWpNasIp() {
		return wpNasIp;
	}
	public void setWpNasIp(String wpNasIp) {
		this.wpNasIp = wpNasIp;
	}
	public Long getWpNasPort() {
		return wpNasPort;
	}
	public void setWpNasPort(Long wpNasPort) {
		this.wpNasPort = wpNasPort;
	}
	public String getApMac() {
		return apMac;
	}
	public void setApMac(String apMac) {
		this.apMac = apMac;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getNasIpv6() {
		return nasIpv6;
	}
	public void setNasIpv6(String nasIpv6) {
		this.nasIpv6 = nasIpv6;
	}
	public String getUserIpv6() {
		return userIpv6;
	}
	public void setUserIpv6(String userIpv6) {
		this.userIpv6 = userIpv6;
	}
	public Integer getListDefaultShow() {
		return listDefaultShow;
	}
	public void setListDefaultShow(Integer listDefaultShow) {
		this.listDefaultShow = listDefaultShow;
	}
	public String getUserInfoUuid() {
		return userInfoUuid;
	}
	public void setUserInfoUuid(String userInfoUuid) {
		this.userInfoUuid = userInfoUuid;
	}

}
