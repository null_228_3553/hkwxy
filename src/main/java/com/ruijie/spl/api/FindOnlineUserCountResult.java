package com.ruijie.spl.api;

public class FindOnlineUserCountResult extends SamApiBaseResult{
	private int onlineUserCount;
	
	public FindOnlineUserCountResult(){
		
	}
	
	public FindOnlineUserCountResult(int errorCode,String errorMessage){
		super(errorCode,errorMessage);
	}
	
	public FindOnlineUserCountResult(int errorCode,String errorMessage,int onlineUserCount){
		super(errorCode,errorMessage);
		this.onlineUserCount=onlineUserCount;
	}
	
	public int getOnlineUserCount() {
		return onlineUserCount;
	}

	public void setOnlineUserCount(int onlineUserCount) {
		this.onlineUserCount = onlineUserCount;
	}
	

}
