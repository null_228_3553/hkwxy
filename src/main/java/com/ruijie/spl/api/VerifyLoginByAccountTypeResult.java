package com.ruijie.spl.api;

public class VerifyLoginByAccountTypeResult extends SamApiBaseResult {
	private String userId;
	public VerifyLoginByAccountTypeResult() {
		
	}
	
	public VerifyLoginByAccountTypeResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public VerifyLoginByAccountTypeResult(int errorCode, String errorMessage,String userId ) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.userId =userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
