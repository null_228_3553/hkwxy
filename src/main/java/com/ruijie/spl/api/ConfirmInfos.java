package com.ruijie.spl.api;

public class ConfirmInfos {
	private String confirmTitleForOperator;
	private String confirmTitleForModifyInfluence;
	private String confirmInfosForOperator;
	private String confirmInfosForModifyInfluence;
	
	
	public String getConfirmTitleForOperator() {
		return confirmTitleForOperator;
	}
	public void setConfirmTitleForOperator(String confirmTitleForOperator) {
//		this.confirmTitleForOperator = confirmTitleForOperator;
		this.confirmTitleForOperator = "变更后可用运营商";
		
	}
	public String getConfirmTitleForModifyInfluence() {
		return confirmTitleForModifyInfluence;
	}
	public void setConfirmTitleForModifyInfluence(String confirmTitleForModifyInfluence) {
//		this.confirmTitleForModifyInfluence = confirmTitleForModifyInfluence;
		this.confirmTitleForModifyInfluence = "变更影响";
		
	}
	public String getConfirmInfosForOperator() {
		return confirmInfosForOperator;
	}
	public void setConfirmInfosForOperator(String confirmInfosForOperator) {
		this.confirmInfosForOperator = confirmInfosForOperator;
	}
	public String getConfirmInfosForModifyInfluence() {
		return confirmInfosForModifyInfluence;
	}
	public void setConfirmInfosForModifyInfluence(String confirmInfosForModifyInfluence) {
		this.confirmInfosForModifyInfluence = confirmInfosForModifyInfluence;
	}
	
	
	
}