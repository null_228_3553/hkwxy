package com.ruijie.spl.api;

public class SendMessageResult extends SamApiBaseResult{
private boolean isMessageSent;

public SendMessageResult(){
	
}

public SendMessageResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
}

public SendMessageResult(int errorCode,String errorMessage,boolean isMessageSent){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.isMessageSent=isMessageSent;
}

public boolean isMessageSent() {
	return isMessageSent;
}

public void setMessageSent(boolean isMessageSent) {
	this.isMessageSent = isMessageSent;
}
}
