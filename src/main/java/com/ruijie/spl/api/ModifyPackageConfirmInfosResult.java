package com.ruijie.spl.api;

public class ModifyPackageConfirmInfosResult extends SamApiBaseResult {
	private String msg[];
	
	public ModifyPackageConfirmInfosResult() {

	}
	
	public ModifyPackageConfirmInfosResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;


	}
	
	public ModifyPackageConfirmInfosResult(int errorCode, String errorMessage, String [] msg) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.msg = msg;
	}


	public String [] getMsg() {
		return msg;
	}

	public void setMsg(String[] msg) {
		this.msg = msg;
	}




}
