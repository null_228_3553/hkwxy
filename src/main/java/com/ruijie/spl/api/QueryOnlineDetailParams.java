package com.ruijie.spl.api;

import java.util.Date;

public class QueryOnlineDetailParams extends SamApiBaseParams{
	private Date fromLoginTime;
	private Date toLoginTime;
	private Date fromLogoutTime;
	private Date toLogoutTime;
	private int offSet;
	private int limit;
	//�����ֶ�
	private String reserved0;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	private String reserved4;
	private String reserved5;
	private String reserved6;
	private String reserved7;
	private String reserved8;
	private String reserved9;

	public Date getFromLoginTime() {
		return fromLoginTime;
	}

	public void setFromLoginTime(Date fromLoginTime) {
		this.fromLoginTime = fromLoginTime;
	}

	public Date getToLoginTime() {
		return toLoginTime;
	}

	public void setToLoginTime(Date toLoginTime) {
		this.toLoginTime = toLoginTime;
	}

	public Date getFromLogoutTime() {
		return fromLogoutTime;
	}

	public void setFromLogoutTime(Date fromLogoutTime) {
		this.fromLogoutTime = fromLogoutTime;
	}

	public Date getToLogoutTime() {
		return toLogoutTime;
	}

	public void setToLogoutTime(Date toLogoutTime) {
		this.toLogoutTime = toLogoutTime;
	}

	public int getOffSet() {
		return offSet;
	}

	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getReserved0() {
		return reserved0;
	}

	public void setReserved0(String reserved0) {
		this.reserved0 = reserved0;
	}

	public String getReserved1() {
		return reserved1;
	}

	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}

	public String getReserved2() {
		return reserved2;
	}

	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}

	public String getReserved3() {
		return reserved3;
	}

	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}

	public String getReserved4() {
		return reserved4;
	}

	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}

	public String getReserved5() {
		return reserved5;
	}

	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}

	public String getReserved6() {
		return reserved6;
	}

	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}

	public String getReserved7() {
		return reserved7;
	}

	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}

	public String getReserved8() {
		return reserved8;
	}

	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}

	public String getReserved9() {
		return reserved9;
	}

	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}
}
