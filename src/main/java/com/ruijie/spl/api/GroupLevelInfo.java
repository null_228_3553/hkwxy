package com.ruijie.spl.api;

public class GroupLevelInfo {
private String groupinfoId;
private String groupinfoParentId;
public GroupLevelInfo(){
	
}
public GroupLevelInfo(String groupinfoId,String groupinfoParentId){
	this.groupinfoId=groupinfoId;
	this.groupinfoParentId=groupinfoParentId;
}
public String getGroupinfoId() {
	return groupinfoId;
}
public void setGroupinfoId(String groupinfoId) {
	this.groupinfoId = groupinfoId;
}
public String getGroupinfoParentId() {
	return groupinfoParentId;
}
public void setGroupinfoParentId(String groupinfoParentId) {
	this.groupinfoParentId = groupinfoParentId;
}

}
