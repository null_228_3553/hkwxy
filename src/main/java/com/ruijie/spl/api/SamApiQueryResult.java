package com.ruijie.spl.api;

public class SamApiQueryResult<T> extends SamApiBaseResult{
	public T [] data;
	public int total;

	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public T[] getData() {
		return data;
	}
	public void setData(T[] data) {
		this.data = data;
	}
}
