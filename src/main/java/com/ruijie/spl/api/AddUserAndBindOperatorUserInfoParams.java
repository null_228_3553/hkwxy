package com.ruijie.spl.api;


public class AddUserAndBindOperatorUserInfoParams extends AddUserParams {
	
	private String operatorsUserId; // 用户运营商串号
	private String operatorsUserPassword; // 用户运营商串号密码
	private String operatorsName; // 用户运营商名称

	public String getOperatorsUserId() {
		return operatorsUserId;
	}

	public void setOperatorsUserId(String operatorsUserId) {
		this.operatorsUserId = operatorsUserId;
	}

	public String getOperatorsUserPassword() {
		return operatorsUserPassword;
	}

	public void setOperatorsUserPassword(String operatorsUserPassword) {
		this.operatorsUserPassword = operatorsUserPassword;
	}

	public String getOperatorsName() {
		return operatorsName;
	}

	public void setOperatorsName(String operatorsName) {
		this.operatorsName = operatorsName;
	}

}