package com.ruijie.spl.api;

public class QueryOnlineDetailResultV2 extends SamApiBaseResult{
	private OnlineDetailInfoV2[] onlindetailInfo;
	private int total;
	public QueryOnlineDetailResultV2(){
		
	}
	
	public QueryOnlineDetailResultV2(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.total=0;
		this.onlindetailInfo=null;
	}
	
	public OnlineDetailInfoV2[] getOnlindetailInfo() {
		return onlindetailInfo;
	}

	public void setOnlindetailInfo(OnlineDetailInfoV2[] onlindetailInfo) {
		this.onlindetailInfo = onlindetailInfo;
	}

	public QueryOnlineDetailResultV2(int errorCode,String errorMessage,int total,OnlineDetailInfoV2[] onlindetailInfoV2){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.total=total;
		this.onlindetailInfo=onlindetailInfoV2;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
