package com.ruijie.spl.api;

public class QueryLoginFailLogResult extends SamApiBaseResult {
	private int total;
	private LoginFailLog[] loginFailLog;

	public QueryLoginFailLogResult() {

	}
	
	public QueryLoginFailLogResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.total = 0;
		this.loginFailLog = null;
	}
	
	public QueryLoginFailLogResult(int errorCode, String errorMessage, int total, LoginFailLog[] loginFailLog) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.total = total;
		this.loginFailLog = loginFailLog;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public LoginFailLog[] getLoginFailLog() {
		return loginFailLog;
	}

	public void setLoginFailLog(LoginFailLog[] loginFailLog) {
		this.loginFailLog = loginFailLog;
	}
}
