package com.ruijie.spl.api;

import java.math.BigDecimal;
import java.util.Date;

public class ModifyUserParams {
	private String userId;
	private String  userName;
	private int sex;
	private int freeAuthen;
	private int certType;
	private String  certNo;
	private int  edu;
	private String  address;
	private String postCode;
	private String  tel;
	private String  mobile;
	private String  email;
	private String  userIp;
//	private String  userMac;
//	private String  nasIp;
//	private int nasPort;
	private String  authorIp;
	private String  authorIpV6;//add
	private String  filterId;
	private String  loginInfo;
	private int  userPrivilege;
	private int   userVlan;
	private String  baclName;
	private String vlanName;//add
	private String  selfServPrem;
	private Date autologicDestroyTime;
//	private int type;
	private BigDecimal periodTimeCumut;//add
	private BigDecimal periodTrafficCumut;//add
	private BigDecimal periodForeUpCumut;//add
	private BigDecimal periodForeDownCumut;//add
	private BigDecimal periodInlandUpCumut;//add
	private BigDecimal periodInlandDownCumut;//add
	private BigDecimal periodNtdFlowSumCumut;//add
	private int ipV6MulticastOption;//add
	private String ipV6MulticastAddress;//add
	private int ipV4MulticastOption;//add
	private String ipV4MulticastAddress;//add
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	private String reserved0;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	private String reserved4;
	private String reserved5;
	private String reserved6;
	private String reserved7;
	private String reserved8;
	private String reserved9;
	//add filed by ww
	private boolean canOverDraft_Login; 
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public int getFreeAuthen() {
		return freeAuthen;
	}
	public void setFreeAuthen(int freeAuthen) {
		this.freeAuthen = freeAuthen;
	}
	public int getCertType() {
		return certType;
	}
	public void setCertType(int certType) {
		this.certType = certType;
	}
	public String getCertNo() {
		return certNo;
	}
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	public int getEdu() {
		return edu;
	}
	public void setEdu(int edu) {
		this.edu = edu;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
//	public String getUserMac() {
//		return userMac;
//	}
//	public void setUserMac(String userMac) {
//		this.userMac = userMac;
//	}
//	public String getNasIp() {
//		return nasIp;
//	}
//	public void setNasIp(String nasIp) {
//		this.nasIp = nasIp;
//	}
//	public int getNasPort() {
//		return nasPort;
//	}
//	public void setNasPort(int nasPort) {
//		this.nasPort = nasPort;
//	}
	public String getAuthorIp() {
		return authorIp;
	}
	public void setAuthorIp(String authorIp) {
		this.authorIp = authorIp;
	}
	public String getFilterId() {
		return filterId;
	}
	public void setFilterId(String filterId) {
		this.filterId = filterId;
	}
	public String getLoginInfo() {
		return loginInfo;
	}
	public void setLoginInfo(String loginInfo) {
		this.loginInfo = loginInfo;
	}
	public int getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(int userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public int getUserVlan() {
		return userVlan;
	}
	public void setUserVlan(int userVlan) {
		this.userVlan = userVlan;
	}
	public String getBaclName() {
		return baclName;
	}
	public void setBaclName(String baclName) {
		this.baclName = baclName;
	}
	public String getSelfServPrem() {
		return selfServPrem;
	}
	public void setSelfServPrem(String selfServPrem) {
		this.selfServPrem = selfServPrem;
	}
	public Date getAutologicDestroyTime() {
		return autologicDestroyTime;
	}
	public void setAutologicDestroyTime(Date autologicDestroyTime) {
		this.autologicDestroyTime = autologicDestroyTime;
	}
//	public int getType() {
//		return type;
//	}
//	public void setType(int type) {
//		this.type = type;
//	}
//	public BigDecimal getAccountFee() {
//		return accountFee;
//	}
//	public void setAccountFee(BigDecimal accountFee) {
//		this.accountFee = accountFee;
//	}
//	public String getAccountId() {
//		return accountId;
//	}
//	public void setAccountId(String accountId) {
//		this.accountId = accountId;
//	}
//	public int getAccountState() {
//		return accountState;
//	}
//	public void setAccountState(int accountState) {
//		this.accountState = accountState;
//	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
	public String getField13() {
		return field13;
	}
	public void setField13(String field13) {
		this.field13 = field13;
	}
	public String getField14() {
		return field14;
	}
	public void setField14(String field14) {
		this.field14 = field14;
	}
	public String getField15() {
		return field15;
	}
	public void setField15(String field15) {
		this.field15 = field15;
	}
	public String getField16() {
		return field16;
	}
	public void setField16(String field16) {
		this.field16 = field16;
	}
	public String getField17() {
		return field17;
	}
	public void setField17(String field17) {
		this.field17 = field17;
	}
	public String getField18() {
		return field18;
	}
	public void setField18(String field18) {
		this.field18 = field18;
	}
	public String getField19() {
		return field19;
	}
	public void setField19(String field19) {
		this.field19 = field19;
	}
	public String getField20() {
		return field20;
	}
	public void setField20(String field20) {
		this.field20 = field20;
	}
	public String getReserved0() {
		return reserved0;
	}
	public void setReserved0(String reserved0) {
		this.reserved0 = reserved0;
	}
	public String getReserved1() {
		return reserved1;
	}
	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}
	public String getReserved2() {
		return reserved2;
	}
	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}
	public String getReserved3() {
		return reserved3;
	}
	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}
	public String getReserved4() {
		return reserved4;
	}
	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}
	public String getReserved5() {
		return reserved5;
	}
	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}
	public String getReserved6() {
		return reserved6;
	}
	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}
	public String getReserved7() {
		return reserved7;
	}
	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}
	public String getReserved8() {
		return reserved8;
	}
	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}
	public String getReserved9() {
		return reserved9;
	}
	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}
	public String getAuthorIpV6() {
		return authorIpV6;
	}
	public void setAuthorIpV6(String authorIpV6) {
		this.authorIpV6 = authorIpV6;
	}
	public String getVlanName() {
		return vlanName;
	}
	public void setVlanName(String vlanName) {
		this.vlanName = vlanName;
	}
	public BigDecimal getPeriodTimeCumut() {
		return periodTimeCumut;
	}
	public void setPeriodTimeCumut(BigDecimal periodTimeCumut) {
		this.periodTimeCumut = periodTimeCumut;
	}
	public BigDecimal getPeriodTrafficCumut() {
		return periodTrafficCumut;
	}
	public void setPeriodTrafficCumut(BigDecimal periodTrafficCumut) {
		this.periodTrafficCumut = periodTrafficCumut;
	}
	public BigDecimal getPeriodForeUpCumut() {
		return periodForeUpCumut;
	}
	public void setPeriodForeUpCumut(BigDecimal periodForeUpCumut) {
		this.periodForeUpCumut = periodForeUpCumut;
	}
	public BigDecimal getPeriodForeDownCumut() {
		return periodForeDownCumut;
	}
	public void setPeriodForeDownCumut(BigDecimal periodForeDownCumut) {
		this.periodForeDownCumut = periodForeDownCumut;
	}
	public BigDecimal getPeriodInlandUpCumut() {
		return periodInlandUpCumut;
	}
	public void setPeriodInlandUpCumut(BigDecimal periodInlandUpCumut) {
		this.periodInlandUpCumut = periodInlandUpCumut;
	}
	public BigDecimal getPeriodInlandDownCumut() {
		return periodInlandDownCumut;
	}
	public void setPeriodInlandDownCumut(BigDecimal periodInlandDownCumut) {
		this.periodInlandDownCumut = periodInlandDownCumut;
	}
	public BigDecimal getPeriodNtdFlowSumCumut() {
		return periodNtdFlowSumCumut;
	}
	public void setPeriodNtdFlowSumCumut(BigDecimal periodNtdFlowSumCumut) {
		this.periodNtdFlowSumCumut = periodNtdFlowSumCumut;
	}
	public int getIpV6MulticastOption() {
		return ipV6MulticastOption;
	}
	public void setIpV6MulticastOption(int ipV6MulticastOption) {
		this.ipV6MulticastOption = ipV6MulticastOption;
	}
	public String getIpV6MulticastAddress() {
		return ipV6MulticastAddress;
	}
	public void setIpV6MulticastAddress(String ipV6MulticastAddress) {
		this.ipV6MulticastAddress = ipV6MulticastAddress;
	}
	public int getIpV4MulticastOption() {
		return ipV4MulticastOption;
	}
	public void setIpV4MulticastOption(int ipV4MulticastOption) {
		this.ipV4MulticastOption = ipV4MulticastOption;
	}
	public String getIpV4MulticastAddress() {
		return ipV4MulticastAddress;
	}
	public void setIpV4MulticastAddress(String ipV4MulticastAddress) {
		this.ipV4MulticastAddress = ipV4MulticastAddress;
	}
	public boolean isCan_overdraft_login() {
		return canOverDraft_Login;
	}
	public void setCan_overdraft_login(boolean canOverDraft_Login) {
		this.canOverDraft_Login = canOverDraft_Login;
	}


}
