package com.ruijie.spl.api;

public class FindOnlineStateResult extends SamApiBaseResult{
	private boolean isOnline;
	public FindOnlineStateResult(){
		
	}
	public FindOnlineStateResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	
	public FindOnlineStateResult(int errorCode,String errorMessage,boolean isOnline){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.isOnline=isOnline;
	}
	
	public boolean isOnline() {
		return isOnline;
	}
	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

}
