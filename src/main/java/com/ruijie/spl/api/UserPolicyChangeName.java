package com.ruijie.spl.api;

import java.util.Date;


public class UserPolicyChangeName {
	
	
	private Date cancelTime;
	private String createHost;
	private Date createTime;
	private String lastUpdateHost;
	private Date lastUpdateTime;
	private String newPolicyInfoId;
	private String newUserTemplateId;
	private String newUserpackageId;
	private String oldPolicyInfoId;
	private String oldUserpackageId;
	private Short operationTimeType;
	private String operatorId;
	private int processState;
	private Date processTime;  
	private int sourceType;
	private String userId;
	
	public Date getCancelTime() {
		return cancelTime;
	}
	
	public void setCancelTime(Date cancelTime){
		this.cancelTime = cancelTime;
	}
	
	public String getCreateHost() {
		return createHost;
	}
	public void setCreateHost(String createHost) {
		this.createHost = createHost;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getLastUpdateHost() {
		return lastUpdateHost;
	}
	public void setLastUpdateHost(String lastUpdateHost) {
		this.lastUpdateHost = lastUpdateHost;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public Short getOperationTimeType() {
		return operationTimeType;
	}
	public void setOperationTimeType(Short operationTimeType) {
		this.operationTimeType = operationTimeType;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public int getProcessState() {
		return processState;
	}
	public void setProcessState(int processState) {
		this.processState = processState;
	}
	public Date getProcessTime() {
		return processTime;
	}
	public void setProcessTime(Date processTime) {
		this.processTime = processTime;
	}
	public int getSourceType() {
		return sourceType;
	}
	public void setSourceType(int sourceType) {
		this.sourceType = sourceType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getNewPolicyInfoId() {
		return newPolicyInfoId;
	}
	public void setNewPolicyInfoId(String newPolicyInfoId) {
		this.newPolicyInfoId = newPolicyInfoId;
	}
	public String getNewUserTemplateId() {
		return newUserTemplateId;
	}
	public void setNewUserTemplateId(String newUserTemplateId) {
		this.newUserTemplateId = newUserTemplateId;
	}
	public String getNewUserpackageId() {
		return newUserpackageId;
	}
	public void setNewUserpackageId(String newUserpackageId) {
		this.newUserpackageId = newUserpackageId;
	}
	public String getOldPolicyInfoId() {
		return oldPolicyInfoId;
	}
	public void setOldPolicyInfoId(String oldPolicyInfoId) {
		this.oldPolicyInfoId = oldPolicyInfoId;
	}
	public String getOldUserpackageId() {
		return oldUserpackageId;
	}
	public void setOldUserpackageId(String oldUserpackageId) {
		this.oldUserpackageId = oldUserpackageId;
	}


}