<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="decription" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>华中大微校园</title>
    <link rel="stylesheet" href="${ctxStatic}/css/icon.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/style.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/color_02.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/mui.css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/myequi.css" />
</head>
<body>
    <header class="header_box key_color">
        <a class="header_home pp_a016 isz_24 left key_color" href="${ctx}/mobile/index"></a>
        华科微校园
        <a class="header_about pp_a017 isz_26 right key_color"></a>
    </header>
    <div class="container-fluid" id="divBody">
        <div id="wrapper">
                <div class="t-btnBox">
                    <button class="mui-btn t-btn device">在线设备</button>
                </div>
                <div class="t-btnBox">
                    <button class="mui-btn t-btn toModifyPassword">密码修改</button>
                </div>
                <div class="t-btnBox">
                    <button class="mui-btn t-btn package">套餐信息</button>
                </div>
                <div class="t-btnBox">
                    <button class="mui-btn t-btn chargeCard">充值卡充值</button>
                </div>
                <div class="t-btnBox">
                    <button class="mui-btn t-btn remind">费用提醒</button>
                </div>
        </div>
    </div>
    <div class="bottom_box_02">
        <div class="bottom_box_inside">
            <span style="width:233px;" id="bottom_p_02">
            智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
        </span>
        </div>
    </div>
    <script src="${ctxStatic}/js/jquery.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/layer.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/mui.js"></script>
    <script type="text/javascript">
	    $('.device').click(function() {
	    	location.href='${ctx}/mobile/device';
	    });
	    $('.toModifyPassword').click(function() {
	    	location.href='${ctx}/mobile/toModifyPassword';
	    });
	    $('.package').click(function() {
	    	location.href='${ctx}/mobile/packageInfo';
	    });
	    $('.chargeCard').click(function() {
	    	location.href='${ctx}/mobile/chargeCard';
	    });
	    $('.remind').click(function() {
	    	location.href='${ctx}/mobile/xywRemind';
	    });
    </script>
</body>
</html>