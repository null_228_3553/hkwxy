<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="decription" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>华中大微校园</title>
    <link rel="stylesheet" href="${ctxStatic}/css/icon.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/style.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/color_02.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/mui.css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/myequi.css" />
</head>

<body>
    <header class="header_box key_color">
        <a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
        套餐变更
        <a class="header_about pp_a017 isz_26 right key_color"></a>
    </header>
    <div class="container-fluid" id="divBody">
        <div id="wrapper" class="packageChange">
            <form action="">
                <input type="hidden" name="userId" value="${userInfo.accountId}"/>
                <input type="hidden" name="userTemplateName" value="${userInfo.atName}"/>
                <ul class="mui-table-view">
                    <li class="mui-table-view-cell">当前套餐<span>${userInfo.packageName}</span><span class="mui-badge t-det">查看详情</span></li>
                </ul>
                <p class="t-ps">请选择要变更的套餐</p>
                <c:forEach items="${packageList}" var="packageInfo">
                  <div class="mui-input-row mui-radio mui-left mui-table-view-cell">
                    <span class="userPackageId">${packageInfo.userPackageId}</span>
                    <input name="radio1" type="radio" <c:if test="${packageInfo.userPackageId == userInfo.packageName}">checked="checked"</c:if> >
                    <span class="mui-badge t-det">查看详情</span>
                  </div>
                </c:forEach>
                <p class="t-ps">请选择套餐生效时间</p>
                <div class="mui-input-row mui-radio mui-left mui-table-view-cell">
                    <span>当前套餐到期后生效</span>
                    <input name="radio2" type="radio" readonly checked="checked">
                </div>
                <div class="t-btnBox">
                    <button class="mui-btn t-btn">变更套餐</button>
                </div>
            </form>
        </div>
    </div>
    <div class="bottom_box_02">
        <div class="bottom_box_inside">
            <span style="width:233px;" id="bottom_p_02">
            智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
        </span>
        </div>
    </div>
    <script src="${ctxStatic}/js/jquery.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/layer.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/mui.js"></script>
    <script type="text/javascript">
    /*提交*/
    $('.t-btn').click(function() {
        var oll = layer.load(2, {});
        var upn = $('input[name=radio1]:checked').closest('div').find('.userPackageId').text();
        $.ajax({
            url: '${ctx}/mobile/changeUserPackage',
            type: 'POST',
            dataType: 'json',
            data: {onPackageName: '${userInfo.packageName}',userId: '${userInfo.accountId }',userTemplateName:'${userInfo.atName}',userPackageName:upn,operationTimeType:2},
            boforeSend:function () {},
            success:function (str) {
                layer.close(oll);
                if (str.errorCode == 0) {
                    layer.open({
                   	  title:'套餐变更成功',
                   	  content: '<div><p>当前套餐：'+str.onPackageName+'</p><p>变更套餐：'+str.userPackageName+'</p><p>生效时间：'+str.activeDate+'</p></div>',
                   	  yes: function(index, layero){
                   		location.href=history.go(-1);
                   	    layer.close(index); //如果设定了yes回调，需进行手工关闭
                   	  }
                   	});
                }else{
                    layer.msg(str.errorMessage);
                }
            },
            error:function (xhr) {
                layer.close(oll);
                layer.msg("错误码:"+xhr.status);
            }
        });
        return false;
    });
    $('#wrapper').on('click', '.mui-table-view-cell', function(event) {
        $(this).find('input').prop('checked',true);
    });
    </script>
</body>

</html>
