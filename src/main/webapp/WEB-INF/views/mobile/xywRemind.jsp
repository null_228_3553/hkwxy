<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="decription" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>华中大微校园</title>
    <link rel="stylesheet" href="${ctxStatic}/css/style.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/elecFee1/css/color_01.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/elecFee1/css/dfcx.css" media="screen" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/icon.css" media="screen" type="text/css" />
    <script src="${ctxStatic}/js/jquery.js" type="text/javascript"></script>
    <script src="${ctxStatic}/elecFee1/js/resetAndroidFontsize.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/layer.js" type="text/javascript"></script>
    <script type="text/javascript">
    var warnSwitch = "${remindInfo.remind}";
    if(warnSwitch=="")
    	warnSwitch="off";
    $(document).ready(function() {
        if (warnSwitch == "on") {
            on();
        } else {
            off();
        }


        $("#OK").click(function() {
            if ($('#warnSwitch').attr('value') == 'on' && !/^[0-9]{1,3}$/.test($("#threshold").val())) {
                $("#errSpan").html("天数只允许输入一到三位数字");
                return;
            } else if (/^[0-9]{1,3}$/.test($("#threshold").val())) {
                $("#errSpan").html("");
            }
            var param={
            		beforeDays:$("#threshold").val(),
            		remind:warnSwitch
            }
            var loadLayer=layer.load(1,{shade: [0.5,'#fff']});
            $.post("${ctx}/mobile/updateRemind",param,function(data){
				layer.close(loadLayer);
				layer.msg('设置成功');
			},"json");
        });
    });

    function changeState(obj) {
        if (obj.getAttribute("class") == "onoff-button on") {
            off();
            warnSwitch="off";
            return;
        }
        if (obj.getAttribute("class") == "onoff-button off") {
            on();
            warnSwitch="on";
            return;
        }
    }

    function off() {
        $("#subscribedBtn").attr("class", "onoff-button off")
        $("#warnSwitch").val("off");
        $("#thresholdLi").hide();
    }

    function on() {
        $("#subscribedBtn").attr("class", "onoff-button on")
        $("#warnSwitch").val("on");
        $("#thresholdLi").show();
    }
    </script>
</head>

<body>
    <header class="header_box key_color">
        <a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
        设置套餐到期提醒
        <a  class="header_about pp_a017 isz_26 right key_color"></a>
    </header>
    <div class="main_02">
        <div class="main_list">
            <ul class="dfcx_box">
                <li class="dfcx_bar">
               		<span class="dfcx_area">姓名：</span>
						<span class="dfcx_area_01">
						${userInfo.userName}
						</span>
                </li>
                <li class="dfcx_bar">
                    <span class="dfcx_area">是否开启提醒</span>
                    <span class="dfcx_area_01" style="float:right;margin-top:10px;">
						<div id="subscribedBtn" class="onoff-button on" onclick="changeState(this)">1</div>
						<input type="hidden" id="warnSwitch" name="warnSwitch" value="on"/>
					</span>
                </li>
                <li class="dfcx_bar" id="thresholdLi">
                    <span class="dfcx_area">到期提前几天提醒</span>
                    <span class="dfcx_area_01" style="float:right">
						<input value="${remindInfo.beforeDays}" defaultValue ="5" id="threshold" name="threshold" type="text" class="dfcx_text_01" style="display:inline;text-align:right;width:150px;color:black" />
						天
					</span>
                </li>
            </ul>
            <span style="color:red;font-size:18px;padding-left:20px;" id="errSpan"></span>
            <div class="button_02_outside button_color margin_top_20">
                <input type="button" id="OK" value="设置" class="button_02" />
            </div>
        </div>
    </div>
    <div class="bottom_box_02">
        <div class="bottom_box_inside">
            <span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
		</span>
        </div>
    </div>
</body>

</html>
