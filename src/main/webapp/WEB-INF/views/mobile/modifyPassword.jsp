<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="decription" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>华中大微校园</title>
    <link rel="stylesheet" href="${ctxStatic}/css/icon.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/style.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/color_02.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/mui.css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/myequi.css" />
</head>

<body>
    <header class="header_box key_color">
        <a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
        修改密码
        <a class="header_about pp_a017 isz_26 right key_color"></a>
    </header>
    <div class="container-fluid" id="divBody">
        <div id="wrapper">
                <div style="color:red;padding-top:10px;padding-left:10px;">此处修改的是校园网密码(不是统一身份认证密码)。</div>
                <ul class="mui-table-view t-ul">
                    <li class="mui-table-view-cell">
                        <span class="t-tit">姓名</span>
                        <span>${userName }</span>
                        <input class="username" type="hidden" value="${userId }">
                    </li>
                    <li class="mui-table-view-cell">
                        <span class="t-tit">原密码</span>
                        <input class="oldp" type="password" maxlength="16" placeholder="8-16位字母和数字">
                    </li>
                    <li class="mui-table-view-cell">
                        <span class="t-tit">新密码</span>
                        <input class="newp1" type="password" maxlength="16" placeholder="8-16位字母和数字">
                    </li>
                    <li class="mui-table-view-cell">
                        <span class="t-tit">确认新密码</span>
                        <input class="newp2" type="password" maxlength="16" placeholder="8-16位字母和数字">
                    </li>
                </ul>
                <div class="t-btnBox">
                    <button class="mui-btn t-btn">确定</button>
                </div>
        </div>
    </div>
    <div class="bottom_box_02">
        <div class="bottom_box_inside">
            <span style="width:233px;" id="bottom_p_02">
            智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
        </span>
        </div>
    </div>
    <script src="${ctxStatic}/js/jquery.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/layer.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/mui.js"></script>
    <script type="text/javascript">
    $('.t-btn').click(function() {
        if ($('.username').val()=='') {
            layer.msg('用户名不能为空');
        }else if ($('.oldp').val()=='') {
            layer.msg('原密码不能为空');
        }else if ($('.newp1').val()=='') {
            layer.msg('新密码不能为空');
        }else if ($('.newp2').val()=='') {
            layer.msg('确认新密码不能为空');
        }else if ($('.newp1').val()!=$('.newp2').val()) {
        	layer.msg('两次输入的密码不同，请重新输入');
        }else if (/^(?=.*[0-9].*)(?=.*[A-Za-z].*).{8,16}$/.test($('.newp1').val())==false){
        	layer.msg('新密码需为8-16数字加字母 组合');
        }else{
            var llo = layer.load(2, {});
            $.ajax({
                url: '${ctx}/mobile/verifyPassword',
                type: 'POST',
                dataType: 'json',
                data: {userId: $('.username').val(),password:$('.oldp').val()},
                boforeSend:function () {},
                success:function (data) {
                    console.log(data);
                    if (data.errorCode == 0) {
                        $.ajax({
                            url: '${ctx}/mobile/modifyPassword',
                            type: 'POST',
                            dataType: 'json',
                            data: {userId: $('.username').val(),password:$('.newp2').val()},
                            boforeSend:function () {},
                            success:function (str) {
                                layer.close(llo);
                                console.log(str);
                                if (str.errorCode == 0) {
                                    layer.msg('恭喜密码修改成功');
                                }else{
                                    layer.msg(str.errorMessage);
                                }
                            },
                            error:function (xhr) {
                                layer.msg("错误码:"+xhr.status);
                            }
                        });
                    }else{
                        layer.close(llo);
                        layer.msg(data.errorMessage);
                    }
                },
                error:function (xhr) {
                    layer.close(llo);
                    layer.msg("错误码:"+xhr.status);
                }
            });
        }
    });
    $('.newp1').blur(function() {
        if ($('.newp2').val()!=''&&$(this).val()!=$('.newp2').val()) {
            layer.msg('两次输入的密码不同，请重新输入');
        }
    });
    $('.newp2').blur(function() {
        if ($(this).val()!=$('.newp1').val()) {
            layer.msg('两次输入的密码不同，请重新输入');
        }
    });
    </script>
</body>
</html>
