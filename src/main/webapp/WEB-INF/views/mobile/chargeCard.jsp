<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="decription" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>华中大微校园</title>
    <link rel="stylesheet" href="${ctxStatic}/css/icon.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/style.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/color_02.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/mui.css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/myequi.css" />
</head>

<body>
    <header class="header_box key_color">
        <a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
        充值服务
        <a class="header_about pp_a017 isz_26 right key_color"></a>
    </header>
    <div class="container-fluid" id="divBody">
        <div id="wrapper">
            <form>
                <ul class="mui-table-view t-ul">
                    <li class="mui-table-view-cell">
                        <span class="t-tit">充值用户</span>
                        <input name="userId" type="text" class="t-edit" value="${userInfo.accountId}">
                    </li>
                    <li class="mui-table-view-cell">
                        <span class="t-tit">充值卡卡号</span>
                        <input name="cardNo" type="text" placeholder="请输入充值卡卡号">
                    </li>
                    <li class="mui-table-view-cell">
                        <span class="t-tit">充值卡密码</span>
                        <input name="password" type="password" placeholder="请输入充值卡密码">
                    </li>
                </ul>
                <div class="t-btnBox">
                    <button class="mui-btn t-btn">余额充值</button>
                </div>
            </form>
        </div>
    </div>
    <div class="bottom_box_02">
        <div class="bottom_box_inside">
            <span style="width:233px;" id="bottom_p_02">
            智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
        </span>
        </div>
    </div>
    <script src="${ctxStatic}/js/jquery.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/layer.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/mui.js"></script>
    <script type="text/javascript">
	$('.t-btn').click(function(){
		if($('input[name=userId]').val()==''){
			layer.msg('请输入充值用户');
		}else if($('input[name=cardNo]').val()==''){
			layer.msg('请输入充值卡卡号');
		}else if($('input[name=password]').val()==''){
			layer.msg('请输入充值卡密码');
		}else{
			var formdata = $('form').serialize();
			
            var oll = layer.load(2, {});
			$.ajax({
	            url: '${ctx}/mobile/chargeUserCard',
	            type: 'POST',
	            dataType: 'json',
	            data: formdata,
	            boforeSend:function () {},
	            success:function (data) {
	            	console.log(data);
	                layer.close(oll);
	                if (data.errorCode == 0) {
	                	$('input[name=cardNo]').val('');
	                	$('input[name=password]').val('');
	                	
		                var canOverdraft = data.accountInfo.canOverdraft;
		                if(canOverdraft){
		                	canOverdraft = '是';
		                }else{
		                	canOverdraft = '否';
		                }
		                var overdraftFee = data.accountInfo.overdraftFee;
		                if(overdraftFee==null){
		                	overdraftFee = '无';
		                }
		                var overdraftFeeLeft = data.accountInfo.overdraftFeeLeft;
		                if(overdraftFeeLeft==null){
		                	overdraftFeeLeft = '无';
		                }
		                var accountState = data.accountInfo.accountState;
		                if(accountState==1){
		                	accountState = '正常';
		                }else if(accountState==2){
		                	accountState = '透支';
		                }else if(accountState==3){
		                	accountState = '欠费';
		                }else if(accountState==100){
		                	accountState = '余额不足';
		                }
	                	
	                    layer.open({
	                    	title:'充值成功',
	                    	content:'<div><p>账户名：'+data.accountInfo.accountId+'</p><p>是否可透支：'+canOverdraft+'</p><p>信用额度(元)：'+overdraftFee+'</p><p>可用额度(元)：'+overdraftFeeLeft+'</p><p>账户状态：'+accountState+'</p><p>账户余额：'+data.accountInfo.accountFee+'</p><p>待扣款余额：'+data.accountInfo.preAccountFee+'</p></div>'
	                    });
	                }else{
	                    layer.msg(data.errorMessage);
	                }
	            },
	            error:function (xhr) {
	                layer.close(oll);
	                layer.msg("错误码:"+xhr.status);
	            }
	        });
			return false;
		}
	});
    </script>
</body>
</html>
