<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="decription" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>华中大微校园</title>
    <link rel="stylesheet" href="${ctxStatic}/css/icon.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/style.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/color_02.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/mui.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/myequi.css" type="text/css" />
</head>

<body>
    <header class="header_box key_color">
        <a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
        我的设备
        <a class="header_about pp_a017 isz_26 right key_color"></a>
    </header>
    <div class="container-fluid" id="divBody">
        <div id="wrapper">
            <c:choose>
              <c:when test="${empty onlineUserList}">
                <div class="nulldata" style="text-align: center;padding-top: 20px;">
	                <p style="font-size: 20px;">您当前没有设备在线</p>
<%-- 	                <a href="${ctx}/mobile/index" style="color:#38a3d9;">返回到首页</a> --%>
	            </div>
              </c:when>
              <c:otherwise>
                <c:forEach items="${onlineUserList}" var="onlineUser">
	              <ul class="mui-table-view">
	                <li class="mui-table-view-cell">设备<span class="mui-badge t-offline" did="${onlineUser.userId }">强制下线</span></li>
	                <li class="mui-table-view-cell">上线时间<span class="mui-badge mui-badge-inverted">${onlineUser.onlineTime }</span></li>
	                <li class="mui-table-view-cell">IP地址<span class="mui-badge mui-badge-inverted">${onlineUser.userIpv4 }</span></li>
	              </ul>
	            </c:forEach>
              </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="bottom_box_02">
        <div class="bottom_box_inside">
            <span style="width:233px;" id="bottom_p_02">
            智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
        </span>
        </div>
    </div>
    <script src="${ctxStatic}/js/jquery.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/layer.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/mui.js"></script>
    
    <script type="text/javascript">
    /*下线点击*/
    $('#wrapper').on('click', '.t-offline', function() {
        var that = this;
        mui.confirm("确认该设备下线吗？", '请注意', ['确认', '取消'], function (e) {
                if (e.index == 0) {
                    /*下线执行操作*/
                    var userId = $(that).attr('did');
                    var oll = layer.load(2, {});
                    $.ajax({
                        url:'${ctx}/mobile/killOnlineUser',
                        cache:false,
                        type:"POST",
                        dataType:'json',
                        data:{userId:userId},
                        boforeSend:function () {},
                        success:function (data) {
                        	layer.close(oll);
                        	console.log(data);
                        	if (data.errorCode == 0) {
                        		$(that).closest('ul').remove();
                        		layer.msg('恭喜用户下线成功!');
                        	}else{
                        		layer.msg(data.errorMessage);
                        	}
                        },
                        error:function (xhr) {
                        	layer.close(oll);
                        	layer.msg("错误码:"+xhr.status);
                        }
                    });
                }
            });
    });
    </script>
</body>
</html>
