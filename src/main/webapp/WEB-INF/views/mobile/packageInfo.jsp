<%@page contentType="text/html;charset=UTF-8" %>
<%@include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="decription" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>华中大微校园</title>
    <link rel="stylesheet" href="${ctxStatic}/css/icon.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/style.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/color_02.css" type="text/css" />
    <link rel="stylesheet" href="${ctxStatic}/css/mui.css" />
    <link rel="stylesheet" href="${ctxStatic}/myequi/css/myequi.css" />
</head>

<body>
    <header class="header_box key_color">
        <a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
                我的套餐
        <a class="header_about pp_a017 isz_26 right key_color"></a>
    </header>
    <div class="container-fluid" id="divBody">
        <div id="wrapper" class="packageInfo">
            <h4>${userInfo.packageName}</h4>
            <div class="progressbarBox">
                <div class="t-progressbar">
                    <p style="width:${userInfo.ratio}%;" <c:if test="${userInfo.ratio>90}">class="active"</c:if>><span>${userInfo.surplusDay}天</span></p>
                </div>
                <span class="start mui-pull-left">${userInfo.startDate}</span>
                <span class="end mui-pull-right">${userInfo.endDate}</span>
            </div>
            <div class="info">
                <div class="">
                    <p>余额</p>
                    <span><strong>${userInfo.accountFee}</strong>元</span>
                </div>
                <div class="">
                    <p>到期时间</p>
                    <strong>${userInfo.expireDate}</strong> 
                </div>
            </div>
            
            <div class="t-btnBox">
                <button class="mui-btn t-btn">套餐变更</button>
            </div>
        </div>
    </div>
    <div class="bottom_box_02">
        <div class="bottom_box_inside">
            <span style="width:233px;" id="bottom_p_02">
            智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
            </span>
        </div>
    </div>
    <script src="${ctxStatic}/js/jquery.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/layer.js" type="text/javascript"></script>
    <script src="${ctxStatic}/js/mui.js"></script>
    <script type="text/javascript">
    $('.t-btn').click(function() {
    	location.href = "${ctx}/mobile/changePackage";
    });
    </script>
</body>
</html>
